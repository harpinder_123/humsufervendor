import AsyncStorage from '@react-native-async-storage/async-storage';
import { request, requestGet, requestMultipart } from './ApiSauce';
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const AspectRatio = () => width / height;

// import { Subject } from 'rxjs';
// import { debounceTime } from 'rxjs';

const Api = {
  signin: json => request('/vendor/signin', json),
  checkEmailPhone: json => request('/vendor/check_email_phone', json),
  signup: json => request('/vendor/signupvender', json),
  otp: json => request('/verify/otp_signup', json),
  home: json => request('/vendor/home', json),
  dynamicProcess: () => request('/vendor/dynamic_process'),
  acceptrejectorder: json => request('/vendor/acceptrejectorder', json),
  assignDriverList: json => request('/vendor/assign_driver', json),
  startOrder: json => request('/vendor/startorder', json),
  completedOrder: json => request('/vendor/completedorder', json),
  driverProfileUpdateApi: json => request('/vendor/driverprofileupdate', json),
  driverQuantityUpdateApi: json => request('/vendor/driver_quantity', json),
  driverDeleteApi: json => request('/vendor/driver_delete', json),
  driverORSealCodeApi: json => request('/vendor/scancode', json),
  notificationApi: json => request('/listnotification', json),
  profileUpdate: json => request('/vendor/profile-update-vendor', json),
  updateFuelPriceApi: json => request('/vendor/update_fuel_price', json),
  addDriverApi: form => requestMultipart('/vendor/adddriver', form),
  transactionHistory: json => request('/vendor/driver_fuel_transaction_history', json),
  transactionHistoryVander: json => request('/vendor/fuel_transaction_history', json),
  driverList: json => requestGet('/vendor/driverlist', json),
  getProfile: () => requestGet('/vendor/getprofile'),
  getMOC: () => requestGet('/omc'),
  mastersettingsApi: () => requestGet('/mastersettings'),
  fuelUpdatePriceApi: () => requestGet('/vendor/checkfuelpriceupdated'),
  vehicletypeApi: () => requestGet('/vehicletype'),
  cancelreasonApi: () => requestGet('/cancelreason'),
};

const LocalStorage = {
  setFcmToken: fcmToken => AsyncStorage.setItem('fcmToken', fcmToken),
  getFcmToken: () => AsyncStorage.getItem('fcmToken'),
  setToken: token => AsyncStorage.setItem('authToken', token),
  getToken: () => AsyncStorage.getItem('authToken'),
  setUserDetail: user_detail => AsyncStorage.setItem('userdata', user_detail),
  getUserDetail: () => AsyncStorage.getItem('userdata'),
  setRandomNum: val => AsyncStorage.setItem('random', val),
  getRandomNum: () => AsyncStorage.getItem('random'),
  clear: AsyncStorage.clear,
};

//const onSearchEvent = new Subject().pipe(debounceTime(500));

export { width, height, Api, LocalStorage };
