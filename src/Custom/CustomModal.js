import React, {memo} from 'react';
import {Modal, StyleSheet, Text, View} from 'react-native';

export const CustomModal = memo(props => (
  <Modal
    // animationType="
    transparent={true}
    visible={props.visible}
    statusBarTranslucent={true}
    onRequestClose={() => {
      console.log('Modal has been closed.');
    }}>
    <View style={styles.container} />
    <View style={[styles.container, props.contentContainerStyle]} {...props}>
      {/* <View style={styles.modalView}>{props.children}</View> */}
    </View>
  </Modal>
));

export default CustomModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000026',
  },
  modalView: {
    marginTop: -20,
    backgroundColor: '#FFFFFF',
    padding: 10,
  },
});
