import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ImageBackground, Alert } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { app_name } from '../services/Config';

// const { uploadImage, uploadImg } = this.props;

export default class CustomCropImagePicker extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }

    selectCamera = () => {
        Alert.alert(
            app_name,
            'chouse your camera and gallery',
            [
                { text: 'Camera', onPress: () => { this.launchCamera(); console.log('OK Pressed') } },
                { text: 'Gallery', onPress: () => { this.launchLibrary(); console.log('OK Pressed') } },
                { text: 'Cancel', onPress: () => { 'cancel' }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    launchCamera = () => {
        console.log('---------image 1: ');
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
            mediaType: 'photo',
            compressImageQuality: 0.6,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log('---------image Camera: ', image);
            let arry = {
                'uri': image.path,
                'name': Math.round(new Date().getTime() / 1000) + 'test.jpg',
                'type': image.mime,
            };
            this.props.dataGet(arry);
        });
    }

    launchLibrary = () => {
        ImagePicker.openPicker({
            cropping: true,
            mediaType: 'photo',
            compressImageQuality: 0.6,
            freeStyleCropEnabled: true,
        }).then((images) => {
            console.log('-----images Gallery:  ', images)
            let arry = {
                'uri': images.path,
                'name': Math.round(new Date().getTime() / 1000) + '----test.jpg',
                'type': "image/jpeg",
                // 'name': 'image.jpg',
                // 'type': 'images.jpg',
            };
            this.props.dataGet(arry);
            this.forceUpdate();
        });
    };

    render() {
        const { uploadImage } = this.props;
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.touchOffCss}
                onPress={() => { this.launchLibrary(); }}>
                <ImageBackground
                    source={require('../images/vendor-app-slice/Rectangle.png')}
                    style={styles.image}>
                    <Image
                        source={require('../images/vendor-app-slice/upload.png')}
                        style={styles.subimage} />
                    <Text style={styles.text}>Upload</Text>
                </ImageBackground>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    touchOffCss: { justifyContent: 'center', },
    image: {
        width: 150,
        height: 99,
        marginRight: 20,
        marginBottom: 10,
        resizeMode: 'contain'
    },
    subimage: {
        width: 24,
        height: 30,
        alignSelf: 'center',
        marginTop: 15,
    },
    text: {
        fontFamily: 'Aviner-Meduim',
        fontSize: 14,
        fontWeight: '600',
        color: '#1E1F20',
        alignSelf: 'center',
        marginTop: 5,
    },
});
