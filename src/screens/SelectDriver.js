import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {RadioButton} from 'react-native-paper';

const {height, width} = Dimensions.get('window');
const ReturnJerryCan = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);
  const [checked, setChecked] = useState('first');

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Assign Driver:</Text>
          </View>

          <View style={styles.topv4_Style}>
            <View>
              <Text style={styles.text2_Style}>Sahil Sharma</Text>
              <Text style={styles.subtext2_Style}>DL 11 4S 2765</Text>
            </View>
            {/* <RadioButton
              value="first"
              status={checked === 'first' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('first')}
              uncheckedColor={'#69707F'}
              color={'#ED6E1E'}
            /> */}
          </View>
          <View style={[styles.line, {width: '100%', marginLeft: 0}]} />

          <View style={styles.topv4_Style}>
            <View>
              <Text style={styles.text2_Style}>Vinay Yadav</Text>
              <Text style={styles.subtext2_Style}>DL 01 4S 4512</Text>
            </View>
            <RadioButton
              value="Second"
              status={checked === 'Second' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('Second')}
              uncheckedColor={'#69707F'}
              color={'#ED6E1E'}
            />
          </View>
          <View style={[styles.line, {width: '100%', marginLeft: 0}]} />

          <View style={styles.topv4_Style}>
            <View>
              <Text style={styles.text2_Style}>Sunil Sharma </Text>
              <Text style={styles.subtext2_Style}>DL 01 4S 4512</Text>
            </View>
            <RadioButton
              value="Third"
              status={checked === 'Third' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('Third')}
              uncheckedColor={'#69707F'}
              color={'#ED6E1E'}
            />
          </View>
          <View style={[styles.line, {width: '100%', marginLeft: 0}]} />

          <View style={styles.topv4_Style}>
            <View>
              <Text style={styles.text2_Style}>Deepak Kumar </Text>
              <Text style={styles.subtext2_Style}>DL 01 4S 4512</Text>
            </View>
            <RadioButton
              value="Fourth"
              status={checked === 'Fourth' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('Fourth')}
              uncheckedColor={'#69707F'}
              color={'#ED6E1E'}
            />
          </View>
          <View style={[styles.line, {width: '100%', marginLeft: 0}]} />

          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.mdbottomView}
            onPress={() => {
              navigation.navigate('DrawerNavigator');
              setModalOpen(false);
            }}>
            <Text style={styles.mdBottomText}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ReturnJerryCan;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 4.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1: {
    backgroundColor: '#31B9EB',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  mdmiddle: {
    backgroundColor: '#ED6E1E',
    padding: 10,
    width: width / 3,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage: {
    width: 95,
    height: 42,
    resizeMode: 'contain',
    marginTop: 10,
  },
  mdText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 5,
  },
  mdmiddle_1: {
    backgroundColor: '#00000030',
    padding: 10,
    width: width / 2.5,
    borderRadius: 10,
    marginTop: 20,
  },
  mdImage_1: {
    width: 40,
    height: 48,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 10,
  },
  mdmiddleView: {
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-around',
  },
  mdTopText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 12,
  },
  mdbottomView: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 25,
    marginBottom: 20,
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  subtext2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  line: {
    borderColor: '#00000020',
    borderWidth: 0.5,
    marginTop: 15,
  },
});
