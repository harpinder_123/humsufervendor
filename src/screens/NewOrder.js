import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity, Modal, FlatList, ActivityIndicator, Image
} from 'react-native';
import { StatusBarDark } from '../Custom/CustomStatusbar';
import { Header } from '../Custom/CustomView';
import { BottomButton } from '../Custom/CustomView';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import Animated from 'react-native-reanimated';
import { Api } from '../services/Api';
import { RadioButton } from 'react-native-paper';
import { useIsFocused } from '@react-navigation/native';
import { EventRegister } from 'react-native-event-listeners';
import * as SoundHandler from '../services/SoundHandler';
import Toast from 'react-native-simple-toast';

const { height, width } = Dimensions.get('window');

// "http://www.sample-videos.com/audio/mp3/india-national-anthem.mp3

const NewOrder = ({ navigation, route }) => {
  const [orderData, setOrderData] = useState(route.params?.orderId)
  const [assignModalVisible, setAssignModalVisible] = useState(false)
  const [isLodingModal, setIsLodingModal] = useState(false);
  const [isLodingModal2, setIsLodingModal2] = useState(false);
  const [assignIndex, setAssignIndex] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [assignDriverQuantity, setAssignDriverQuantity] = useState('');
  const [checked, setChecked] = useState(null);
  const [assignDriverId, setAssignDriverId] = useState();
  const [selectReject, setselectReject] = useState('');
  const [assignDriverData, setAssignDriverData] = useState([]);
  const [reasonData, setReasonData] = useState([])

  const isFocused = useIsFocused();

  useEffect(() => {
    //listener =
    EventRegister.addEventListener('myCustomEvent', data => {
      get();
    });
    get();
  }, []);

  useEffect(() => {
    SoundHandler.play();
    assignDriverList()
    getReasonApi();
    // SoundHandler.stop();
  }, []);

  const getReasonApi = async () => {
    const response = await Api.cancelreasonApi();
    const { status = false, data = [] } = response;
    if (status) {
      setReasonData(data)
    }
  }

  const get = () => { }

  const acceptrejectorderApi = async () => {
    let body = {
      orderid: orderData.id,
      decis: '1',
    }
    console.log('----------Accepts body: ', body);
    const response = await Api.acceptrejectorder(body);
    const { status = false, msg } = response;
    if (status) {
      Toast.show(msg)
      setAssignModalVisible(false);
      SoundHandler.stop();
      navigation.navigate('DrawerNavigator')
      // navigation.goBack()
      console.log('----------Accepts res: ', response);
    } else {
      console.log('----------Accepts else: ', response);
    }
  }

  const acceptrejectorderCancelApi = async () => {
    if (!selectReject) {
      Toast.show('Please select your reason')
      return;
    } else {
      let body = {
        orderid: orderData.id,
        decis: "2",
        cancel_reason: selectReject
      }
      const response = await Api.acceptrejectorder(body);
      const { status = false, msg } = response;
      if (status) {
        Toast.show(msg)
        SoundHandler.stop();
        rejectModalFunction(false);
        navigation.navigate('DrawerNavigator')
        console.log('----------Cancelled: ', response);
      }
    }
  }

  const callBackFunction = () => {
    SoundHandler.stop();
    navigation.goBack();
    console.log('-----: ', 'call Back');
  }

  const selectDriverFunction = (driverID, driverIndex) => {
    setAssignDriverId(driverID)
    setAssignIndex(driverIndex)
  }

  const assignDriverList = async () => {
    setIsLodingModal(true)
    const response = await Api.driverList();
    const { status = false, drivers = [] } = response;
    if (status) {
      setIsLodingModal(false)
      setAssignDriverData(drivers);
    }
  };

  const submitFunction = async () => {
    if (assignIndex === null) {
      Toast.show('Please select Driver');
      return;
    } else {
      if (orderData.quantity <= assignDriverQuantity) {
        const body = {
          order_id: orderData.id,
          driver_id: assignDriverId
        };
        console.log('-----boby: ', body);
        setIsLodingModal2(true);
        const response = await Api.assignDriverList(body);
        const { status = false, msg } = response;
        if (status) {
          setIsLodingModal2(false);
          setModalOpen(false);
          acceptrejectorderApi();
          console.log('-----assignDriverList: ', response);
        } else {
          Toast.show(msg);
          setIsLodingModal2(false);
          console.log('-----msg else: ', msg);
        }
      } else {
        Toast.show('Sorry ! Driver does not have sufficent fuel. Please add quantity first.')
        setAssignDriverQuantity('');
      }
    }
  }


  const assignModalfunctionModal = () => {
    return (
      <Modal
        visible={assignModalVisible}
        transparent={true}
        onRequestClose={() => {
          setAssignModalVisible(false)
        }}>
        <View style={styles.modalViewOffCss}>
          <View style={styles.modalCardViewOffCss}>
            <View style={styles.assignDriViewOffCss}>
              <Text style={styles.assignDriverTextOffCss}>Assign Driver:</Text>
            </View>
            <View style={{ marginHorizontal: 15, paddingVertical: 10, }}>
              {isLodingModal ?
                <View style={{ flex: 1, height: 60, justifyContent: 'center', backgroundColor: 'red' }}>
                  <ActivityIndicator size={'small'} color='#000' />
                </View>
                :
                <View style={assignDriverData.length > 8 && styles.modalDiverListStyles}>
                  <FlatList
                    data={assignDriverData}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                      return (
                        <TouchableOpacity
                          activeOpacity={0.7}
                          style={styles.driverViewOffCss}
                          onPress={() => { selectDriverFunction(item.id, index), setAssignDriverQuantity(item.quantity) }}>
                          <View>
                            <Text style={[styles.assignDriverTextOffCss, { color: '#9F9F9F' }]}>{item.name}</Text>
                            <Text style={[styles.assignDriverTextOffCss, { color: '#9F9F9F', fontSize: 14 }]}>{item.user_info.vehicle_number}</Text>
                          </View>
                          <View style={{ justifyContent: 'center' }}>
                            <Image style={{ width: 20, height: 20, resizeMode: 'contain' }}
                              source={
                                assignIndex === index ?
                                  require('../images/radio.png')
                                  :
                                  require('../images/unradio.png')
                              } />
                          </View>
                        </TouchableOpacity>
                      )
                    }}
                  />
                </View>
              }
              <View style={{ paddingVertical: 10, marginHorizontal: 10, marginTop: 10 }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => { submitFunction() }}
                  style={styles.submitButtonViewOff}>
                  {isLodingModal2 ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                      <ActivityIndicator size={'small'} color='#FFF' />
                    </View>
                    :
                    <Text style={styles.submitTextOffCss}>SUBMIT</Text>
                  }
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal >
    )
  }

  const appectFunction = () => {
    setAssignModalVisible(true);
    console.log('yes i am appect Function');
  }

  const rejectModalFunction = (visible) => {
    setModalOpen(visible)
  }

  const Reject = () => {
    return (
      <Modal
        visible={modalOpen}
        transparent={true}
        onRequestClose={() => {
          rejectModalFunction(false);
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <View style={styles.topView}>
              <Text style={styles.text1_Style}>Reject Reason:</Text>
              <FlatList
                data={reasonData}
                renderItem={({ item, index }) => {
                  return (
                    <TouchableOpacity
                      style={styles.topv4_Style}
                      onPress={() => { setChecked(index), setselectReject(item.reason) }}>
                      <RadioButton
                        value="first"
                        status={checked === index ? 'checked' : 'unchecked'}
                        onPress={() => { setChecked(index), setselectReject(item.reason) }}
                        uncheckedColor={'#69707F'}
                        color={'#ED6E1E'}
                      />
                      <Text style={styles.text2_Style}>{item.reason}</Text>
                    </TouchableOpacity>
                  )
                }}
              />
            </View>
            <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'flex-end' }}>
              <View>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => { rejectModalFunction(false) }}>
                  <Text style={styles.mdBottomText}>DISMISS</Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginHorizontal: 30 }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => { acceptrejectorderCancelApi() }}>
                  <Text style={[styles.mdBottomText, { color: '#ED6E1E' }]}>SUBMIT</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <StatusBarDark />
      <View style={{ alignSelf: 'center', marginTop: '50%' }}>
        <CountdownCircleTimer
          initialRemainingTime={180}
          size={210}
          strokeWidth={13}
          isPlaying
          duration={180}
          colors="#ED6E1E">
          {({ remainingTime, animatedColor }) => (
            <Animated.Text
              style={{
                color: animatedColor,
                fontFamily: 'Avenir-Heavy',
                fontSize: 50,
                fontWeight: '900',
              }}>
              {remainingTime > 0 ? remainingTime : callBackFunction()} s
            </Animated.Text>
          )}
        </CountdownCircleTimer>
      </View>
      {Reject()}
      {assignModalfunctionModal()}
      <Text style={styles.text}>New Order #{orderData.id}</Text>
      <Text style={styles.subtext}>You have 3 mins to accept the order</Text>
      <Text style={styles.subtext}>{orderData.delivery_address_detail?.address}</Text>
      <Text style={styles.subtext}>Quantity: {orderData.quantity} Ltr</Text>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity activeOpacity={0.7} onPress={() => { rejectModalFunction(true); }}>
          <View style={styles.touch}>
            <Text style={styles.touchtext}>CANCEL</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.7} onPress={() => { appectFunction(); }}>
          <View style={styles.touched}>
            <Text style={styles.touchedtext}>ACCEPT</Text>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};
export default NewOrder;
const styles = StyleSheet.create({
  modalDiverListStyles: {
    height: height / 1.3
  },
  image: {
    height: 243,
    width: 243,
    alignSelf: 'center',
    marginTop: 140,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#080040',
    marginTop: 20,
    textAlign: 'center',
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#8A94A3',
    marginTop: 10,
    textAlign: 'center',
    paddingHorizontal: 40
  },
  touch: {
    borderRadius: 20,
    borderWidth: 2,
    width: 100,
    height: 42,
    marginTop: 20,
    marginHorizontal: 90,
    borderColor: '#E02020',
    justifyContent: 'center',
    alignItems: 'center'
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#E02020',
  },
  touched: {
    borderRadius: 20,
    width: 100,
    height: 42,
    marginTop: 20,
    marginLeft: -80,
    backgroundColor: '#ED6E1E',
    justifyContent: 'center',
    alignItems: 'center'
  },
  touchedtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
  },
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1, justifyContent: 'center',
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    // marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#E02020',
    textAlign: 'center',
  },
  topView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 15,
    paddingBottom: 20,
  },
  text1_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    color: '#080040',
    fontWeight: 'bold',
    marginHorizontal: 20,
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 10,
  },
  text2_Style: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#6E7584',
    fontWeight: '500',
  },
  modalViewOffCss: {
    backgroundColor: '#00000080', flex: 1, justifyContent: 'center',
  },
  modalCardViewOffCss: {
    backgroundColor: '#fff', elevation: 5, marginHorizontal: 20, borderRadius: 5
  },
  assignDriViewOffCss: {
    paddingVertical: 10, paddingHorizontal: 10, backgroundColor: '#31B9EB',
    borderTopLeftRadius: 5, borderTopRightRadius: 5,
  },
  assignDriverTextOffCss: {
    fontFamily: 'Avenir-Heavy', fontSize: 20, fontWeight: '900', color: '#FFFFFF',
  },
  driverViewOffCss: {
    marginHorizontal: 10, flexDirection: 'row', justifyContent: 'space-between',
    borderBottomColor: '#C8C8D3', borderBottomWidth: 0.6, paddingVertical: 10
  },
  submitButtonViewOff: {
    backgroundColor: '#ED6E1E', borderRadius: 20, paddingVertical: 10,
    justifyContent: 'center', alignItems: 'center', height: 42,
  },
  submitTextOffCss: {
    color: '#FFFFFF', fontSize: 16, fontWeight: '900', fontFamily: 'Avenir-Heavy'
  },
});
