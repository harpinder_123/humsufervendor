import React, { useEffect, useState } from 'react';
import {
    Dimensions,
    Image,
    ImageBackground,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native-paper';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { Header } from '../Custom/CustomView';
import { Api } from '../services/Api';
import Toast from 'react-native-simple-toast';

const SCREEN_HEIGHT = Dimensions.get('screen').height

const UpdateFuelPrice = ({ navigation, route }) => {
    const [type, setType] = useState(route?.params?.type)
    const [isLoader, setIsLoader] = useState(true)
    const [isLoaderUpdate, setIsLoaderUpdate] = useState(false)
    const [reload, setReload] = useState(false)
    const [multipleData, setMultipleData] = useState([])
    const [multipleTypeDta, setmultipleTypeData] = useState([])

    useEffect(() => {
        fuelUpdatePrice()
    }, [])

    const fuelUpdatePrice = async () => {
        const response = await Api.fuelUpdatePriceApi();
        const { status = false } = response;
        if (status) {
            setIsLoader(false)
            setMultipleData(response.fueltypes)
        }
    }

    const updateFuelPriceFunction = async () => {
        let body = {
            fuel_price: multipleTypeDta
        }
        setIsLoaderUpdate(true)
        const response = await Api.updateFuelPriceApi(body);
        const { status = false, list, msg } = response;
        if (status) {
            if (type === 'login') {
                setIsLoaderUpdate(false)
                Toast.show(msg);
                navigation.replace('DrawerNavigator');
            } else {
                setIsLoaderUpdate(false)
                Toast.show(msg);
                navigation.goBack();
            }
        } else {
            Toast.show(msg);
            setIsLoaderUpdate(false)
        }

    }

    const addFuelList = ({ item, index }) => {
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={styles.textItemOffCss}>{item}</Text>
                <View style={styles.textInputView}>
                    <TextInput
                        returnKeyType="next"
                        keyboardType={'number-pad'}
                        style={styles.textInputText}
                        value={multipleTypeDta[index]}
                        onChangeText={text => {
                            setReload({ reload: !reload })
                            multipleTypeDta[index] = { fuel_type: item, price: text };
                        }}
                    />
                </View>
            </View>
        )
    }

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1, backgroundColor: '#fff' }}>
            <StatusBarLight />
            <View style={styles.headerVIew}>
                <Text style={styles.headerText}>Diesel Price</Text>
            </View>
            {isLoader ?
                <View style={{ height: SCREEN_HEIGHT / 1.4, justifyContent: 'center' }}>
                    <ActivityIndicator size={'small'} color={'#000'} />
                </View>
                : multipleData === undefined ?
                    <View style={styles.loaderViewOff}>
                        <Text style={styles.notFoundTextOff}>Not Available</Text>
                    </View>
                    :
                    <>
                        <View style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 10 }}>
                            <Text style={styles.updateText}>Please update your fuel price </Text>
                            <FlatList
                                data={multipleData}
                                renderItem={addFuelList}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                        <View style={{ flex: 0.1, justifyContent: 'center', marginHorizontal: 20, marginVertical: 20 }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                disabled={multipleTypeDta.length >= multipleData.length ? false : true}
                                style={[styles.touchButton, {
                                    backgroundColor: multipleTypeDta.length >= multipleData.length ? '#ED6E1E' : '#00000030'
                                }]}
                                onPress={() => { updateFuelPriceFunction() }}>
                                {isLoaderUpdate ?
                                    <View style={{ flax: 1 }}>
                                        <ActivityIndicator size={'small'} color={'#fff'} />
                                    </View>
                                    :
                                    <Text style={[styles.textButton, {
                                        color: multipleTypeDta.length >= multipleData.length ? '#FFF' : '#00000050'
                                    }]}>UPDATE</Text>
                                }
                            </TouchableOpacity>
                        </View>
                    </>

            }
        </ScrollView >
    );
};
export default UpdateFuelPrice;

const styles = StyleSheet.create({
    headerVIew: {
        backgroundColor: '#31B9EB', height: 85, alignItems: 'center', justifyContent: 'center'
    },
    headerText: {
        fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 20, color: '#FFFFFF', top: 15,
    },
    updateText: {
        fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 16, color: '#0E1236',
    },
    textInputView: {
        borderColor: '#fff', borderWidth: 0.4, borderRadius: 8, elevation: 4, height: 48,
        backgroundColor: '#fff', marginTop: 15, marginBottom: 5, marginHorizontal: 1, justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.3,
        //   shadowRadius: 60,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    textItemOffCss: {
        fontFamily: 'Avenir-Medium', fontWeight: '500', fontSize: 17, color: '#000', paddingHorizontal: 10
    },
    textInputText: {
        fontFamily: 'Avenir-Medium', fontWeight: '500', fontSize: 17, color: '#000', paddingHorizontal: 10
    },
    touchButton: {
        alignItems: 'center', paddingVertical: 13, borderRadius: 25,
    },
    textButton: {
        fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 16, color: '#FFFFFF',
    },
    loaderViewOff: {
        height: SCREEN_HEIGHT / 1.4, justifyContent: 'center', alignItems: 'center'
    },
    notFoundTextOff: {
        fontSize: 16, fontWeight: '500', color: '#000'
    }
});
