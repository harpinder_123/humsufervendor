import * as React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { StatusBarLight } from '../Custom/CustomStatusbar';

const RegistApproval = ({ navigation }) => {

    return (
        <View style={styles.modal_View}>
            <StatusBarLight />
            <View style={styles.mdtop}>
                <Image style={{ width: 50, height: 50, resizeMode: 'contain', marginVertical: 7, marginTop: 13 }} source={require('../images/tick.png')} />
                <Text style={[styles.registerTextOffCss, { marginBottom: 5 }]}>Registration submitted for</Text>
                <Text style={styles.registerTextOffCss}>approval.</Text>
                <Text style={[styles.registerText2OffCss, { marginBottom: 5, }]}>After approval from admin you can</Text>
                <Text style={styles.registerText2OffCss}>use the app.</Text>
            </View>
        </View>
    );
};
export default RegistApproval;

const styles = StyleSheet.create({
    modal_View: {
        flex: 1,
        backgroundColor: '#777777',
        justifyContent: 'center',
        alignItems: 'center',
    },
    mdtop: {
        width: "90%",
        backgroundColor: '#FFFFFF',
        borderRadius: 15,
        alignItems: 'center',
        bottom: "2%"
    },
    registerTextOffCss: {
        color: '#1E1F20', fontFamily: 'Avenir-Heavy', fontWeight: '700', fontSize: 18,
        textAlign: 'center', marginBottom: 20, paddingHorizontal: 40,
    },
    registerText2OffCss: {
        color: '#6F6F7B', fontFamily: 'Avenir-Heavy', fontWeight: '600', fontSize: 15,
        textAlign: 'center', marginBottom: 20, paddingHorizontal: 40
    }
});
