import React, { useEffect, useState } from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { BottomButton, BottomView, MainImage } from '../Custom/CustomView';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomInputField } from './Login';
import { Api, LocalStorage } from '../services/Api';
import { _SetAuthToken } from '../services/ApiSauce';

const Register1 = ({ navigation, route }) => {
  const [personalDetails, setPersonalDetails] = useState(route.params?.item)
  const [state, setState] = useState({
    Bankname: '',
    AccountHolderName: '',
    BranchName: '',
    AccountNumber: '',
    IfscCode: '',
    isLoading: false,
    devicetoken: '',
    setValidation: false,
  });

  useEffect(() => {
    // getCurrentPosition();
    LocalStorage.getFcmToken('fcmToken').then((fcmToken) => {
      console.log('-----fcmToken: ', fcmToken)
      setState({ ...state, devicetoken: fcmToken })
    })
  }, [])

  const onSubmitHandler = async () => {
    const {
      Bankname, AccountHolderName, BranchName,
      AccountNumber, IfscCode, devicetoken
    } = state;

    if (Bankname === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (AccountHolderName === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (BranchName === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (AccountNumber === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (IfscCode === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    const body = {
      name: personalDetails.name,
      contact_person: personalDetails.contact_person,
      proprietor_name: personalDetails.proprietor_name,
      email: personalDetails.email,
      omc: personalDetails.omc,
      city: personalDetails.city,
      state: personalDetails.state,
      phone: personalDetails.phone,
      paytm_number: personalDetails.paytm_number,
      tin_no: personalDetails.tin_no,
      gst_no: personalDetails.gst_no,
      pincode: personalDetails.pincode,
      bank_name: Bankname,
      account_holder_name: AccountHolderName,
      branch_name: BranchName,
      account_number: AccountNumber,
      ifsc_code: IfscCode,
      latitude: '',
      longitude: '',
      selling_petrol: '',
      selling_diesel: '',
      password: '',
      device_type: Platform.OS === 'ios' ? 'IOS' : "Android",
      devicetoken: devicetoken,
    };
    setState({ ...state, isLoding: true });
    navigation.replace('Register2', { item: body });
    setState({ ...state, isLoding: false });
  };
  return (
    <KeyboardAwareScrollView>
      <MainImage height={"100%"} bottom={"0%"}>
        <BottomView>
          <Text style={styles.topText}>Bank Details</Text>
          <Text style={styles.topText_1}>Enter your basic details</Text>

          <CustomInputField
            tfSource={require('../images/bank-building.png')}
            label="Bank Name"
            width={16}
            height={16}
            value={state.Bankname}
            onChangeText={Bankname => setState({ ...state, Bankname })}
          />
          {state.setValidation == true && state.Bankname === '' ?
            <Text style={styles.validationStyle}>Please enter your Bank name</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/man-user.png')}
            label="Account Holder Name"
            width={16}
            height={16}
            value={state.AccountHolderName}
            onChangeText={AccountHolderName =>
              setState({ ...state, AccountHolderName })
            }
          />
          {state.setValidation == true && state.AccountHolderName === '' ?
            <Text style={styles.validationStyle}>Please enter your Account Holder Name</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/three-buildings.png')}
            label="Branch Name"
            width={16}
            height={16}
            value={state.BranchName}
            onChangeText={BranchName => setState({ ...state, BranchName })}
          />
          {state.setValidation == true && state.BranchName === '' ?
            <Text style={styles.validationStyle}>Please enter your Branch name</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/accoundNum.png')}
            label="Account Number"
            width={16}
            height={16}
            value={state.AccountNumber}
            keyboardType={'number-pad'}
            onChangeText={AccountNumber => setState({ ...state, AccountNumber })}
          />
          {state.setValidation == true && state.AccountNumber === '' ?
            <Text style={styles.validationStyle}>Please enter your Account Number</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/omc.png')}
            label="Ifsc Code"
            width={20}
            height={20}
            value={state.IfscCode}
            onChangeText={IfscCode => setState({ ...state, IfscCode })}
          />
          {state.setValidation == true && state.IfscCode === '' ?
            <Text style={styles.validationStyle}>Please enter your Ifsc Code</Text>
            : <></>
          }

          <BottomButton
            loader={state.isLoading}
            bottomtitle="SAVE & NEXT"
            onPress={() => { onSubmitHandler() }}
          />

          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.bottomText}>
              Already have an account?
              <Text style={styles.bottomText_1}> Login Now</Text>
            </Text>
          </TouchableOpacity>
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default Register1;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    marginLeft: 10,
    marginTop: 4,
  },
  otpInput: {
    // width: '84%',
    padding: 10,
    marginHorizontal: 10,
    marginTop: 20,
    height: 66,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#F9F9F9',
    backgroundColor: '#F9F9F9',
    borderRadius: 20,
    fontSize: 35,
    color: '#000000',
  },

  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#ED6E1E',
    fontSize: 35,
    color: '#FFFFFF',
    borderColor: '#E7E7E7',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
    alignSelf: 'center',
    marginTop: 10,
  },
  bottomText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
    marginTop: 4,
  },
  validationStyle: {
    fontFamily: 'Avenir-Medium', fontSize: 14, fontWeight: '500', color: '#FA7272', marginHorizontal: 35
  }
});
