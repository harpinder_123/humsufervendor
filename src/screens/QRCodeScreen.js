import React, { Component } from "react";
import { View, Dimensions, Text, TouchableOpacity, Image, Alert } from "react-native";
import QRCodeScanner from "react-native-qrcode-scanner";
// import Icon from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import { Header } from "../Custom/CustomView";
import { RNCamera } from 'react-native-camera';
import { Api } from "../services/Api";
import Toast from 'react-native-simple-toast';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

class QRCodeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            qrData: '',
            driver_Id: this.props.route.params.driverId,
            isFlashOn: false,
            isReload: true,
        };
    }

    driverSealApi = async (item) => {
        const body = {
            driver_id: this.state.driver_Id,
            code: item.target
        };
        const response = await Api.driverORSealCodeApi(body);
        const { status = false, msg } = response;
        if (status) {
            Alert.alert(
                "Humfer Vendor",
                msg + " and go back to the screen",
                [
                    {
                        text: "Cancel",
                        onPress: () => { console.log("Cancel Pressed") },
                        style: "cancel"
                    },
                    {
                        text: "OK", onPress: () => {
                            this.props.navigation.navigate('MyDrivers')
                        }
                    }
                ]
            );
        } else {
            Toast.show(msg);
        }
    }

    onSuccess(e) {
        this.driverSealApi(e)
    }

    makeSlideOutTranslation(translationType, fromValue) {
        return {
            from: {
                // [translationType]: SCREEN_WIDTH * -0.18
                [translationType]: SCREEN_WIDTH * 0.28
            },
            to: {
                [translationType]: fromValue
            }
        };
    }

    render() {
        return (
            <View styles={styles.containr}>
                <Header onPress={() => { this.props.navigation.goBack() }} title={'QR CODE SCANNER'} />
                <QRCodeScanner
                    showMarker
                    flashMode={this.state.isFlashOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
                    onRead={this.onSuccess.bind(this)}
                    cameraStyle={{ height: SCREEN_HEIGHT }}
                    customMarker={
                        <View style={styles.rectangleContainer}>
                            <View style={styles.topOverlay}>
                                <Text style={{ fontSize: 30, color: "white" }}>
                                    Scan Any QR Code
                                </Text>
                            </View>

                            <View style={{ flexDirection: "row" }}>
                                <View style={styles.leftAndRightOverlay} />
                                <View style={styles.rectangle}>
                                    <Animatable.View
                                        style={styles.scanBar}
                                        direction="alternate-reverse"
                                        iterationCount="infinite"
                                        duration={1700}
                                        easing="linear"
                                        animation={this.makeSlideOutTranslation(
                                            "translateY",
                                            SCREEN_WIDTH * -0.30
                                        )}
                                    />
                                </View>
                                <View style={styles.leftAndRightOverlay} />
                            </View>
                            <View style={styles.bottomOverlay}>
                                <View style={{ marginVertical: 25, justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity
                                        style={{ paddingVertical: 5, }}
                                        onPress={() => { this.setState({ isFlashOn: !this.state.isFlashOn }) }}>
                                        {this.state.isFlashOn === false ?
                                            <Image style={{ width: 30, height: 30, resizeMode: 'contain', marginRight: 10 }} source={require('../images/flash_off.png')} />
                                            :
                                            <Image style={{ width: 30, height: 30, resizeMode: 'contain' }} source={require('../images/flash_on.png')} />
                                        }
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }
                />
            </View>
        );
    }
}

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency

const rectDimensions = SCREEN_WIDTH * 0.65; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = "#31B9EB";

const scanBarWidth = SCREEN_WIDTH * 0.46; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const scanBarColor = "#31B9EB";

const iconScanColor = "blue";

const styles = {
    containr: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    rectangleContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },

    rectangle: {
        height: rectDimensions,
        width: rectDimensions,
        borderWidth: rectBorderWidth,
        borderColor: rectBorderColor,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },

    topOverlay: {
        flex: 1,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor,
        justifyContent: "center",
        alignItems: "center"
    },

    bottomOverlay: {
        flex: 1,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor,
        paddingBottom: SCREEN_WIDTH * 0.25
    },

    leftAndRightOverlay: {
        height: SCREEN_WIDTH * 0.65,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor
    },

    scanBar: {
        width: "88%",
        height: scanBarHeight,
        backgroundColor: scanBarColor
    }
};

export default QRCodeScreen;