import React, { useEffect, useState } from 'react';
import { Image, StatusBar, StyleSheet, TouchableOpacity, View, Platform } from 'react-native';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { MainView } from '../Custom/CustomView';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Pending from './Pending';
import Ongoing from './Ongoing';
import Completed from './Completed';
import Cancelled from './Cancelled';
import { Api } from '../services/Api';
import BackgroundTimer from 'react-native-background-timer';

const Tab = createMaterialTopTabNavigator();

const Home = ({ navigation }) => {

  useEffect(() => {
    const intervalId = BackgroundTimer.setInterval(() => {
      // this will be executed every 200 ms
      // even when app is the the background
      console.log('----App Status Background');
      // const response =  Api.dynamicProcess();
      // const { status = false, orderdata = {} } = response;
      // console.log('-----res: ', response)
      // if (status) {
      //   navigation.navigate('NewOrder', { orderId: orderdata });
      //   console.log('-----res if: ', "abhi me jinda hu");
      // } else {
      //   console.log('-----res else: ', "abhi me jinda nhi hu")
      //   console.log('-----res else: ', response)
      // }
      bookingFuncationApi();
    }, 20000);
    // BackgroundTimer.clearInterval(intervalId);
    navigationHandler()
    getProfileApi()
    const unsubscribe = navigation.addListener('focus', () => {
      navigationHandler();
    });
    return unsubscribe;
  }, [navigation])

  const bookingFuncationApi = async () => {
    const response = await Api.dynamicProcess();
    const { status = false, orderdata = {} } = response;
    if (status) {
      navigation.navigate('NewOrder', { orderId: orderdata });
      console.log('-----res if: ', "abhi me jinda hu");
    } else {
      console.log('-----res else: ', "abhi me jinda nhi hu")
      // navigation.navigate('NewOrder', { orderId: orderdata });
    }
  }

  const navigationHandler = async () => {
    const body = {
      condition: 'upcoming'
    };
    const response = await Api.home(body);
    const { status = false, orders = [] } = response;
  }

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }


  return (
    <MainView>
      {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: '#31B9EB'
        }}>
          <StatusBar barStyle={"light-content"} translucent={true} {...navigation} />
        </View>
        :
        <StatusBarLight />
      }
      <View style={styles.topView}>
        <View style={styles.header}>
          <TouchableOpacity activeOpacity={0.7} onPress={navigation.openDrawer}>
            <Image
              style={styles.headerImage}
              source={require('../images/menu.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
            <Image
              style={styles.headerImage_1}
              source={require('../images/notification.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
      <Tab.Navigator
        tabBarOptions={{
          keyboardHidesTabBar: true,
          //   scrollEnabled: true,
          inactiveTintColor: '#000521',
          activeTintColor: '#ED6E1E',
          style: { backgroundColor: '#FFFFFF', },
          indicatorStyle: { backgroundColor: '#ED6E1E' },
          labelStyle: { width: 80, marginRight: 10, fontFamily: 'Avenir-Medium', fontWeight: 'bold', fontSize: 11.50 }  //  hp(1.23)
        }}>
        <Tab.Screen name="Pending" component={Pending} options={{ tabBarLabel: 'Pending' }} />
        <Tab.Screen name="Ongoing" component={Ongoing} options={{ tabBarLabel: 'Ongoing' }} />
        <Tab.Screen name="Completed" component={Completed} options={{ tabBarLabel: 'COMPLETED' }} />
        <Tab.Screen name="Cancelled" component={Cancelled} options={{ tabBarLabel: 'CANCELLED' }} />
      </Tab.Navigator>
    </MainView>
  );
};

export default Home;

const styles = StyleSheet.create({
  topView: {
    backgroundColor: '#31B9EB',
    padding: 10,
  },
  headerImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  headerImage_1: {
    width: 18,
    height: 20,
    resizeMode: 'contain',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: StatusBar.currentHeight + 10,
    marginHorizontal: 10,
    justifyContent: 'space-between',
  },
});
