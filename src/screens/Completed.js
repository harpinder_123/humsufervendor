import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Dash from 'react-native-dash';
import CustomModal from '../Custom/CustomModal';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { ButtonStyle } from '../Custom/CustomView';
import { Api } from '../services/Api';
import moment from 'moment';

const Completed = ({ navigation }) => {
  const [isLoding, setIsLoding] = useState(true);
  const [completedData, setCompletedData] = useState([]);

  useEffect(() => {
    completedHomeApi();
    const unsubscribe = navigation.addListener('focus', () => {
      completedHomeApi();
    });
    return unsubscribe;
  }, [navigation])

  const completedHomeApi = async () => {
    const body = {
      condition: 'completed'
    };
    const response = await Api.home(body);
    const { status = false, orders = [] } = response;
    setTimeout(() => {
      setIsLoding(false)
      setCompletedData(orders);
    }, 2000);
  };

  const completedFunctionList = ({ item, index }) => {
    moment.locale('en');
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.cardViewOffCss}
        onPress={() => { navigation.navigate("OrderDetail", { cardDetails: item }) }}>
        <View style={styles.ftview}>
          <Text style={styles.ftText}>Order #{item.id}</Text>
          <Text style={styles.ftText1}>{moment(item.delivery_date, "YYYY-MM-DD, h:mm a").format("DD/MM/YYYY - h:mm A")}</Text>
        </View>

        <DashLine />

        <Text style={[styles.ftText2, { marginHorizontal: 10, paddingVertical: 0 }]}>Status:
          <Text style={[styles.ftText2, { color: '#00C327', marginRight: 10, marginLeft: 10, paddingVertical: 2 }]}> Completed</Text>
        </Text>

        <View style={styles.ftview_1}>
          <View>
            <Text style={styles.ftText2}>Name</Text>
            <Text style={styles.ftText2}>Mobile No.</Text>
            <Text style={styles.ftText2}>Delivery Address</Text>
            <Text style={styles.ftText2}>Driver Name</Text>
            <Text style={styles.ftText2}>Truck Number</Text>
            <Text style={styles.ftText2}>Quantity (in Litres)</Text>
          </View>
          <View>
            <Text style={styles.ftText_3}>{item.order_by_detail?.name}</Text>
            <Text style={styles.ftText_3}>{item.order_by_detail?.phone}</Text>
            <Text numberOfLines={1} style={[styles.ftText_3, { width: 180 }]}>{item.delivery_address_detail?.address}</Text>
            <Text style={styles.ftText_3}>{item.driver_details?.name}</Text>
            <Text style={styles.ftText_3}>{item.driver_details?.driver_info?.vehicle_number}</Text>
            <Text style={styles.ftText_3}>{item.quantity} LTR</Text>
          </View>
        </View>

        <DashLine />
        <View style={[styles.ftview_1, { marginTop: 0 }]}>
          <Text style={styles.ftText}>
            Total:
            <Text style={[styles.total, { color: '#F87B00' }]}> ₹{item.total_amount}/-</Text>
          </Text>

          {/* {ButtonStyle('DISPATCH ', '#ED6E1E', '#FFFFFF', () =>
            navigation.navigate('Payment'),
          )} */}
        </View>
      </TouchableOpacity>
    )
  }
  return (
    <View style={{ flex: 1 }}>
      <StatusBarLight />
      {isLoding ?
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size={'large'} color='#000' />
        </View>
        :
        completedData.length > 0 ?
          <FlatList
            data={completedData}
            renderItem={completedFunctionList}
            showsVerticalScrollIndicator={false}
          />
          :
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={styles.notFoundTextOffCss}>Not Available</Text>
          </View>
      }
    </View>
  );
};

export default Completed;

const styles = StyleSheet.create({
  cardViewOffCss: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
    borderRadius: 10,
    marginHorizontal: 20,
    elevation: 4,
    marginVertical: 10
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#9F9F9F',
  },
  // dash: {
  //   borderColor: '#6F6F7B',
  //   marginHorizontal: 20,
  //   borderStyle: 'dotted',
  //   borderWidth: 1,
  //   marginTop: 10,
  // },
  ftText2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#9F9F9F',
    paddingVertical: 2
  },

  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    marginTop: 10,
    alignItems: 'center',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'flex-end',
    paddingVertical: 2
  },
  notFoundTextOffCss: {
    fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 18, color: '#000'
  }
});

export const DashLine = props => (
  <View style={{ margin: 10 }}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
