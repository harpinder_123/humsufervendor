import * as React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {Header} from '../Custom/CustomView';

const DieselPrice = ({navigation}) => {
  return (
    <ScrollView>
      <StatusBarDark />
      <Header onpress={() => navigation.goback()} title="Diesel Price" />
      <Text style={styles.text}>Please update your fuel price</Text>
      <TextInput
        editable={true}
        style={styles.textInput}
        placeholder={'72.56/litre'}
      />

      <TextInput
        editable={true}
        style={styles.textInput}
        placeholder={'72.56/litre'}
      />

      <TouchableOpacity onPress={() => navigation.navigate('NewOrder')}>
        <View style={styles.touch}>
          <Text style={styles.touchtext}>UPDATE</Text>
        </View>
      </TouchableOpacity>
    </ScrollView>
  );
};
export default DieselPrice;
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 20,
    marginTop: 20,
  },
  textInput: {
    borderRadius: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
    margin: 20,
    borderColor: '#ffffff',
    backgroundColor: 'white',
    elevation: 5,
  },
  touch: {
    borderRadius: 25,
    marginTop: '100%',
    marginHorizontal: 30,
    padding: 15,
    backgroundColor: '#ED6E1E',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ffffff',
    textAlign: 'center',
  },
});
