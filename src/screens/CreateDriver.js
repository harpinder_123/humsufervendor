import React, { useState, useRef, useEffect } from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { StatusBarDark, StatusBarLight } from '../Custom/CustomStatusbar';
import { Header, MainView } from '../Custom/CustomView';
import { Api, LocalStorage } from '../services/Api';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';
import CustomCropImagePicker from '../Custom/CustomCropImagePicker';
import { ActivityIndicator } from 'react-native-paper';
import Toast from 'react-native-simple-toast';

// import Loader from '../Custom/Loader';
// import * as actions from '../redux/actions';
// import ImagePicker from 'react-native-image-crop-picker';

const CreateDriver = ({ navigation }) => {
  const [token, setToken] = useState('');
  const [isLoader, setIsLoader] = useState(false)
  const [frontAdharImg, setFrontAdharImg] = useState('');
  const [backAdharImg, setBackAdharImg] = useState('');
  const [frontVoterImg, setFrontVoterImg] = useState('');
  const [backVoterImg, setBackVoterImg] = useState('');
  const [state, setState] = useState({
    name: '', mobileNum: '', vehicleNum: '', vehicleId: '',
    bowserName: '', password: '', isLoading: false,
  });

  const [selectJob, setSelectJob] = useState([]);

  const input_mobileNum = useRef(null)
  const input_vehicleNum = useRef(null)
  const input_vehicleId = useRef(null)
  const input_password = useRef(null)

  useEffect(() => {
    LocalStorage.getToken('authToken').then((authToken) => {
      setToken(authToken)
      getProfileApi()
      getBrowserType();
    })
  }, [])

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }

  const getBrowserType = async () => {
    const response = await Api.vehicletypeApi();
    const { status = false, data = [] } = response;
    if (status) {
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        var obj = { "label": element.name, "value": element.id }
        selectJob.push(obj);
      }
    }
  }

  const onCreateProfileApi = async () => {
    const { name, mobileNum, vehicleNum, vehicleId, bowserName, password } = state;
    if (!name) {
      Toast.show('Please enter your name');
      return false;
    }
    if (!mobileNum) {
      // setState({ ...state, setValidation: true });
      Toast.show('Please enter your Mobile Number')
      return;
    }

    if (mobileNum.length !== 10) {
      // setState({ ...state, setValidation: true });
      Toast.show('Mobile number must be in 10 digits')
      return;
    }

    if (!vehicleNum) {
      Toast.show('Please enter your Vehicle Number');
      return false;
    }

    // if (vehicleNum.length >= 7) {
    //   Toast.show('Vehicle number must be in 7 digits');
    //   return false;
    // }

    if (!vehicleId) {
      Toast.show('Please enter your Vehicle Id');
      return false;
    }
    if (!bowserName) {
      Toast.show('Please enter your Bowser Name');
      return false;
    }
    if (!password) {
      Toast.show('Please enter your password');
      return false;
    }
    if (!frontAdharImg) {
      Toast.show('Please select your Front Adhar Card Image');
      return false;
    }
    if (!backAdharImg) {
      Toast.show('Please select your Back Adhar Card Image');
      return false;
    }
    if (!frontVoterImg) {
      Toast.show('Please select your Front Voter Id Image');
      return false;
    }
    if (!backVoterImg) {
      Toast.show('Please select your Back Voter Id Image');
      return false;
    }

    let formdata = new FormData();
    formdata.append("name", name);
    formdata.append("phone", mobileNum);
    formdata.append("vehicle_number", vehicleNum);
    formdata.append("car_id", vehicleId);
    formdata.append("bowser", bowserName);
    formdata.append("password", password);
    formdata.append("aadharcard_front", {
      uri: frontAdharImg.uri,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    // params.append('image', {
    //   uri: profilePicture,
    //   name: 'image.jpg',
    //   type: 'image/jpeg',
    // });
    formdata.append("aadharcard_back", {
      uri: backAdharImg.uri,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    formdata.append("voter_id_front", {
      uri: frontVoterImg.uri,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    formdata.append("voter_id_back", {
      uri: backVoterImg.uri,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    setIsLoader(true)
    console.log('------body: ', JSON.stringify(formdata));
    const response = await Api.addDriverApi(formdata);
    const { status = false, msg } = response;
    console.log('------response: ', JSON.stringify(response));
    if (status) {
      Toast.show('Successfully');
      setIsLoader(false);
      navigation.goBack();
      console.log('------response: ', JSON.stringify(response));
    } else {
      Toast.show(msg);
      setIsLoader(false);
      console.log('------response.msg : ', JSON.stringify(msg));
    }

    // const requestOptions = {
    //   method: 'POST',
    //   headers: new Headers({
    //     Accept: 'application/json',
    //     'Content-Type': 'multipart/form-data',
    //     'Authorization': `Bearer ${token}`,
    //   }),
    //   body: formdata,
    // };
    // fetch("https://humsafarindia.co.in/Humsafar-Fuel/api/vendor/adddriver", requestOptions)
    //   .then(function (response) {
    //     return response.json();
    //   }).then(function (response) {
    //     if (response.status === true) {
    //       Toast.show('Successfully');
    //       setIsLoader(false);
    //       navigation.goBack();
    //       console.log('------response: ', JSON.stringify(response));
    //     } else {
    //       Toast.show(response.msg);
    //       // const { data: { msg } } = response;
    //       setIsLoader(false);
    //       console.log('------response.msg : ', JSON.stringify(response.msg));
    //     }
    //   })
    // .catch(function (error) {
    //     Toast.show(error);
    //     setIsLoader(false);
    //   });
  }

  const onChangeDropdownUserType = (value, index, data) => {
    setState({ ...state, bowserName: data[index].label });
    console.log('------bowser : ', data[index].label);
  };

  const launchAdharfront = (arry) => {
    setFrontAdharImg(arry)
  }
  const launchAdharBack = (arry) => {
    setBackAdharImg(arry)
  }
  const launchVoterIdfront = (arry) => {
    setFrontVoterImg(arry)
  }
  const launchVoterIdBack = (arry) => {
    setBackVoterImg(arry)
  }


  return (
    <MainView>
      {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: '#31B9EB'
        }}>
          <StatusBar barStyle={"light-content"} translucent={true} {...navigation} />
        </View>
        :
        <StatusBarLight />
      }
      {/* <StatusBarLight /> */}
      <Header onPress={() => navigation.goBack()} title="Create Driver" height={Platform.OS === 'ios' ? 40 : 85} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingTop: 15 }}
      >
        <TextLabel title={'Full Name'} />
        <TextInput
          style={styles.textInput}
          value={state.name}
          returnKeyType="next"
          onChangeText={name => setState({ ...state, name })}
          onSubmitEditing={() => input_mobileNum.current.focus()}
        />
        <TextLabel title={'Mobile No.'} />
        <TextInput
          style={styles.textInput}
          keyboardType={'number-pad'}
          value={state.mobileNum}
          maxLength={10}
          returnKeyType="next"
          onChangeText={mobileNum => setState({ ...state, mobileNum })}
          onSubmitEditing={() => input_vehicleNum.current.focus()}
          ref={input_mobileNum}
        />

        <TextLabel title={'Vehicle No.'} />
        <TextInput
          returnKeyType="next"
          style={styles.textInput}
          value={state.vehicleNum}
          // keyboardType={'visible-password'}
          onChangeText={vehicleNum => setState({ ...state, vehicleNum })}
          onSubmitEditing={() => input_vehicleId.current.focus()}
          ref={input_vehicleNum}
        />
        <TextLabel title={'Browser'} />
        {/* <TextInput
          style={styles.textInput}
        /> */}
        <View style={{ marginTop: '0%', marginBottom: 10 }}>
          <Dropdown
            style={styles.drops}
            itemColor={'rgba(0, 0, 0, .54)'}
            underlineColor="transparent"
            label={''}
            // icon="cheveron-down"
            iconColor="rgba(0, 0, 0, 1)"
            icon={require('../images/next.png')}
            dropdownOffset={{ top: 32, left: 0 }}
            dropdownMargins={{ min: 8, max: 16 }}
            pickerStyle={{ width: '88%', left: '6%', marginTop: 15 }}
            dropdownPosition={-4.5}
            shadeOpacity={0.12}
            rippleOpacity={0.4}
            baseColor={'white'}
            data={selectJob}
            onChangeText={(value, index, data) => {
              onChangeDropdownUserType(value, index, data);
            }}
          />
        </View>

        <TextLabel title={'Vehicle Id'} />
        <TextInput
          style={styles.textInput}
          keyboardType={'number-pad'}
          returnKeyType="next"
          value={state.vehicleId}
          onChangeText={vehicleId => setState({ ...state, vehicleId })}
          onSubmitEditing={() => input_password.current.focus()}
          ref={input_vehicleId}
        />
        <TextLabel title={'Password'} />
        <TextInput
          style={styles.textInput}
          value={state.password}
          returnKeyType="done"
          keyboardType={Platform.OS === 'ios' ? 'default' : 'visible-password'}
          onChangeText={password => setState({ ...state, password })}
          // onSubmitEditing={() => input_password.current.focus()}
          ref={input_password}
        />

        <Text style={styles.uploadeImgTextOffCss}>Upload Aadhar Card</Text>
        <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
          {frontAdharImg === '' ?
            <CustomCropImagePicker
              dataGet={(arry) => { launchAdharfront(arry) }}
            />
            :
            <View style={{ width: 150, height: 99, marginRight: 20, borderRadius: 5, marginBottom: 10 }}>
              <Image
                source={{ uri: frontAdharImg.uri }}
                style={{ width: "100%", height: "100%", borderRadius: 5 }} />
              <TouchableOpacity
                style={styles.closeViewOffCss}
                onPress={() => { setFrontAdharImg('') }}>
                <Image
                  source={require('../images/close.png')}
                  style={{ width: 10, height: 10, resizeMode: 'cover' }} />
              </TouchableOpacity>
            </View>

          }

          {backAdharImg === '' ?
            <CustomCropImagePicker
              dataGet={(arry) => { launchAdharBack(arry) }}
            />
            :
            <View style={{ width: 150, height: 99, borderRadius: 5, marginBottom: 10 }}>
              <Image
                source={{ uri: backAdharImg.uri }}
                style={{ width: "100%", height: "100%", borderRadius: 5, resizeMode: 'cover' }} />
              <TouchableOpacity
                style={styles.closeViewOffCss}
                onPress={() => { setBackAdharImg('') }}>
                <Image
                  source={require('../images/close.png')}
                  style={{ width: 10, height: 10, resizeMode: 'cover' }} />
              </TouchableOpacity>
            </View>
          }
        </View>

        <Text style={styles.uploadeImgTextOffCss}>Upload Voter Id Card</Text>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
            {frontVoterImg === '' ?
              <CustomCropImagePicker
                dataGet={(arry) => { launchVoterIdfront(arry) }}
              />
              :
              <View style={{ width: 150, height: 99, marginRight: 20, borderRadius: 5, marginBottom: 10 }}>
                <Image
                  source={{ uri: frontVoterImg.uri }}
                  style={{ width: "100%", height: "100%", borderRadius: 5, resizeMode: 'cover' }} />
                <TouchableOpacity
                  style={styles.closeViewOffCss}
                  onPress={() => { setFrontVoterImg(''); }}>
                  <Image
                    source={require('../images/close.png')}
                    style={{ width: 10, height: 10, resizeMode: 'contain' }} />
                </TouchableOpacity>
              </View>
            }

            {backVoterImg === '' ?
              <CustomCropImagePicker
                dataGet={(arry) => { launchVoterIdBack(arry) }}
              />
              :
              <View style={{ width: 150, height: 99, borderRadius: 5, marginBottom: 10 }}>
                <Image
                  source={{ uri: backVoterImg.uri }}
                  style={{ width: "100%", height: "100%", borderRadius: 5, resizeMode: 'cover' }} />
                <TouchableOpacity
                  style={styles.closeViewOffCss}
                  onPress={() => { setBackVoterImg('') }}>
                  <Image
                    source={require('../images/close.png')}
                    style={{ width: 10, height: 10, resizeMode: 'cover' }} />
                </TouchableOpacity>
              </View>
            }
          </View>
        </View>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.touchButton}
          onPress={() => { onCreateProfileApi() }}>
          {isLoader ?
            <View>
              <ActivityIndicator size={'small'} color='#fff' />
            </View>
            :
            <Text style={styles.textButton}>CREATE PROFILE</Text>
          }
        </TouchableOpacity>
      </ScrollView>
    </MainView >
  );
};

export default CreateDriver;
const TextLabel = ({ title }) => <Text style={styles.textLabel}>{title}</Text>;
const styles = StyleSheet.create({
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textLabel: {
    fontWeight: '500',
    fontSize: 14,
    color: '#8D92A3',
    marginHorizontal: 20,
    marginTop: 5,
    marginBottom: 0,
  },
  textInput: {
    paddingVertical: 5,
    marginHorizontal: 20,
    fontSize: 16,
    color: '#0E1236',
    // fontFamily: 'Aviner-Meduim',
    fontWeight: '500',
    borderBottomColor: '#00000050',
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  uploadeImgTextOffCss: {
    fontSize: 16,
    color: '#1D252D',
    fontFamily: 'Aviner-Heavy',
    fontWeight: '700',
    paddingHorizontal: 20,
    marginTop: 10,
    marginBottom: 10,
  },
  touchButton: {
    backgroundColor: '#ED6E1E',
    marginHorizontal: 20,
    alignItems: 'center',
    paddingVertical: 13,
    borderRadius: 25,
    marginVertical: 20
  },
  textButton: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#FFFFFF',
  },
  image: {
    width: 130,
    height: 85,
    marginHorizontal: 20,
    marginTop: 5,
    marginBottom: 10
  },
  subimage: {
    width: 24,
    height: 30,
    alignSelf: 'center',
    marginTop: 15,
  },
  drops: {
    height: 50,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    marginHorizontal: 20,
    borderBottomColor: '#00000050',
    // elevation: 2,
  },
  closeViewOffCss: {
    width: 18, height: 18, left: "82%", top: 5, justifyContent: 'center', alignItems: 'center',
    backgroundColor: '#fff', borderRadius: 5, position: 'absolute'
  }
});
