import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import RazorpayCheckout from 'react-native-razorpay';

const onSubmit = () => {
  var options = {
    description: 'Credits towards consultation',
    image: 'https://i.imgur.com/3g7nmJC.png',
    currency: 'INR',
    key: 'rzp_test_RwSgFPpvHSua0O', // Your api key
    amount: '5000',
    name: 'foo',
    prefill: {
      email: 'void@razorpay.com',
      contact: '9191919191',
      name: 'Razorpay Software',
    },
    theme: {color: '#F37254'},
  };
  //   alert(JSON.stringify(options));
  //   return;
  RazorpayCheckout.open(options)
    .then(data => {
      // handle success
      alert(`Success: ${data.razorpay_payment_id}`);
    })
    .catch(error => {
      // handle failure
      alert(`Error: ${error.code} | ${error.description}`);
    });
};

const Razorpay = () => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <TouchableOpacity style={styles.touchButton} onPress={onSubmit}>
        <Text style={styles.textButton}>Click Me</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Razorpay;

const styles = StyleSheet.create({
  touchButton: {
    backgroundColor: '#ED6E1E',
    marginTop: '100%',
    alignItems: 'center',
    paddingVertical: 10,
    marginHorizontal: 100,
    borderRadius: 25,
  },
  textButton: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
});
