import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image, ActivityIndicator, Modal, Keyboard, Platform, StatusBar
} from 'react-native';
import { useStore, useDispatch } from 'react-redux';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { Header } from '../Custom/CustomView';
import { DashLine } from './Pending';
import { Api } from '../services/Api';
import * as actions from '../redux/actions';
import Toast from 'react-native-simple-toast';
import { RadioButton } from 'react-native-paper';

const MyDrivers = ({ navigation }) => {
  const [driversData, setDriverData] = useState([])
  const [quantity, setQuantity] = useState('');
  const [fuelAddID, setFuelAddID] = useState('');
  const [sealCode, setSealCode] = useState('');
  const [modalOpenTopUp, setModalOpenTopUp] = useState(false);
  const [modalOpenScanSeal, setModalOpenScanSeal] = useState(false);
  const [modalOpenManuallyCode, setModalOpenManuallyCode] = useState(false);
  const [modalOpenUpdateFriver, setModalOpenUpdateFriver] = useState(false);
  const [modalOpenDelete, setModalOpenDelete] = useState(false);
  const [checked, setChecked] = useState('');
  const [selectMode, setSelectMode] = useState('scen')
  const [mobileNum, setMobileNum] = useState('');
  const [driverName, setDriverName] = useState('');
  const [vechileId, setVechileId] = useState('');
  const [isLoader, setIsLoader] = useState(true);
  const { drivers } = useStore().getState();
  const dispatch = useDispatch();

  const input_driver = useRef(null)
  const input_number = useRef(null)
  const input_name = useRef(null)
  const input_vechile = useRef(null)

  useEffect(() => {
    navigationHandler();
    getProfileApi();
    const unsubscribe = navigation.addListener('focus', () => {
      navigationHandler();
      getProfileApi();
    });
    return unsubscribe;
  }, [navigation]);

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }

  const navigationHandler = async () => {
    const response = await Api.driverList();
    const { status = false, drivers = [] } = response;
    setTimeout(() => {
      setIsLoader(false)
      setDriverData(drivers);
      dispatch(actions.SetUserDetail(drivers));
    }, 300);
  };

  const upDateDriverQuantiApi = async () => {
    if (!quantity) {
      Toast.show('Please enter your quanitity')
      return false;
    }
    // Keyboard.dismiss()
    const body = {
      driver_id: fuelAddID,
      quantity: quantity
    };
    setIsLoader(true);
    console.log('--------upDateDriverQuantiApi body: ', body);
    const response = await Api.driverQuantityUpdateApi(body);
    const { status = false, msg } = response;
    if (status) {
      setModalOpenTopUp(false);
      setIsLoader(false);
      Toast.show(msg);
      setQuantity('');
      navigationHandler();
      console.log('--------upDateDriverQuantiApi response: ', response);
    } else {
      Toast.show(msg)
      setQuantity('')
      setIsLoader(false);
      setModalOpenTopUp(false);
      console.log('--------upDateDriverQuantiApi else: ', msg);
    }
  }

  const driverSealApi = async () => {
    if (!sealCode) {
      Toast.show('Please enter your seal code');
      return false;
    }
    Keyboard.dismiss()
    const body = {
      driver_id: fuelAddID,
      code: sealCode
    };
    setIsLoader(true);
    console.log('------body: ', body);
    const response = await Api.driverORSealCodeApi(body);
    const { status = false, msg } = response;
    console.log('------response: ', response);
    if (status) {
      setModalOpenManuallyCode(false);
      setIsLoader(false);
      Toast.show(msg);
      setSealCode('');
      navigationHandler();
      console.log('------response if: ', response);
    } else {
      setSealCode('');
      Toast.show(msg);
      setIsLoader(false);
      setModalOpenManuallyCode(false)
      console.log('------response else: ', response);
    }
  }

  const selectOption = () => {
    if (!checked) {
      Toast.show('Please select your option');
      return;
    } else {
      if (selectMode === 'scen') {
        navigation.navigate('QRCodeScreen', { driverId: fuelAddID });
        setModalOpenScanSeal(false);
        setChecked('');
      } else {
        setModalOpenManuallyCode(true);
        setModalOpenScanSeal(false);
        setChecked('');
      }
    }
  }

  const driverDeleteApi = async () => {
    const body = {
      driver_id: fuelAddID,
    };
    setIsLoader(true);
    console.log('------body: ', body);
    const response = await Api.driverDeleteApi(body);
    const { status = false, msg } = response;
    if (status) {
      console.log('------response: ', response);
      setModalOpenDelete(false);
      Toast.show(msg);
      setIsLoader(false);
      navigationHandler();
    } else {
      Toast.show(msg);
      setIsLoader(false);
      setModalOpenDelete(false)
    }
  }

  const upDateProfileDravers = async () => {
    if (!mobileNum) {
      Toast.show('Please enter your Mobile Number');
      return false;
    }
    if (mobileNum.length < 10) {
      Toast.show('Please enter your Mobile Number');
      return false;
    }
    if (!driverName) {
      Toast.show('Please enter your Dariver Name');
      return false;
    }
    if (!vechileId) {
      Toast.show('Please enter your Vechile Id');
      return false;
    }
    const body = {
      driver_id: fuelAddID,
      mobile: mobileNum,
      name: driverName,
      car_id: vechileId
    };
    setIsLoader(true);
    console.log('------body: ', body);
    const response = await Api.driverProfileUpdateApi(body);
    const { status = false, msg } = response;
    if (status) {
      setModalOpenUpdateFriver(false);
      setIsLoader(false);
      Toast.show(msg);
      navigationHandler();
      console.log('------response if: ', response);
    } else {
      const { data: { msg } } = response;
      Toast.show(msg);
      setIsLoader(false);
      setModalOpenUpdateFriver(false);
      console.log('------response: ', response);
    }
  }

  const upDateDriverDetails = () => {
    return (
      <Modal
        visible={modalOpenUpdateFriver}
        transparent={true}
        onRequestClose={() => {
          setModalOpenUpdateFriver(false);
        }}>
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', backgroundColor: '#00000080', }} >
          <View style={styles.mdtop}>
            <View style={styles.mdtop_1UpDate}>
              <Text style={styles.mdTopTextUpDate}>Update Driver</Text>
            </View>
            <Text style={styles.textLabelUpDate}>Driver ID</Text>
            <View style={[styles.textInputUpDate, { paddingVertical: 10, }]}>
              <Text style={{ color: '#000', fontSize: 12, fontWeight: '500' }}>{fuelAddID}</Text>
            </View>

            <Text style={styles.textLabelUpDate}>Mobile Number</Text>
            <TextInput
              style={styles.textInputUpDate}
              placeholder={'Mobile Number'}
              keyboardType={'phone-pad'}
              returnKeyType="next"
              maxLength={10}
              value={mobileNum}
              onChangeText={(text) => { setMobileNum(text) }}
              onSubmitEditing={() => input_number.current.focus()}
              ref={input_name}
            />

            <Text style={styles.textLabelUpDate}>Name</Text>
            <TextInput
              style={styles.textInputUpDate}
              placeholder={'Name'}
              returnKeyType="next"
              value={driverName}
              onChangeText={(text) => { setDriverName(text) }}
              onSubmitEditing={() => input_vechile.current.focus()}
              ref={input_number}
            />

            <Text style={styles.textLabelUpDate}>Vechile</Text>
            <TextInput
              style={styles.textInputUpDate}
              placeholder={'Vechile ID'}
              keyboardType={'phone-pad'}
              returnKeyType="done"
              value={vechileId}
              onChangeText={(text) => { setVechileId(text) }}
              // onSubmitEditing={() => input_number.current.focus()}
              ref={input_vechile}
            />
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.mdbottomViewUpDate}
              onPress={() => { upDateProfileDravers() }}>
              {isLoader ?
                <ActivityIndicator size={'small'} color='#fff' />
                :
                <Text style={styles.mdBottomTextUpDate}>UPDATE</Text>
              }
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Modal>
    );
  }

  const topUpFuncation = () => {
    return (
      <Modal
        visible={modalOpenTopUp}
        transparent={true}
        onRequestClose={() => {
          setModalOpenTopUp(false);
          navigation.navigate('MyDrivers');
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <View style={styles.mdtop_1}>
              <Text style={styles.mdTopText}>Add Fuel</Text>
            </View>

            <TextLabel title={'Add Fuel'} />
            <TextInput
              style={styles.textInput}
              placeholder={'Add Quantity'}
              keyboardType={'phone-pad'}
              maxLength={4}
              value={quantity}
              placeholderTextColor={'#8D92A3'}
              onChangeText={text => { setQuantity(text) }}
            />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 20 }}>
              <TouchableOpacity
                style={styles.touchFuel}
                onPress={() => { setModalOpenTopUp(false) }}>
                <Text style={styles.touchtextFuel}>CANCEL</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.touchedFuel}
                onPress={() => { upDateDriverQuantiApi() }}>
                {isLoader ?
                  <ActivityIndicator size={'small'} color='#fff' />
                  :
                  <Text style={[styles.touchtextFuel, { color: '#ffffff' }]}>SAVE</Text>
                }
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  const scanSealFunction = () => {
    return (
      <Modal
        visible={modalOpenScanSeal}
        transparent={true}
        onRequestClose={() => {
          setModalOpenScanSeal(false);
          navigation.navigate('MyDrivers');
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <View style={styles.mdtop_1}>
              <Text style={styles.mdTopText}>Choose Option:</Text>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.topv4_Style}
              onPress={() => { setChecked('first'), setSelectMode('scen') }}>
              <View>
                <Text style={styles.text2_Style}>Scan Seal</Text>
              </View>
              <RadioButton
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => { setChecked('first'), setSelectMode('scen') }}
                uncheckedColor={'#69707F'}
                color={'#ED6E1E'}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.topv4_Style}
              onPress={() => { setChecked('Second'), setSelectMode('manually') }} >
              <View>
                <Text style={styles.text2_Style}>Enter Manually</Text>
              </View>
              <RadioButton
                value="Second"
                status={checked === 'Second' ? 'checked' : 'unchecked'}
                onPress={() => { setChecked('Second'), setSelectMode('manually') }}
                uncheckedColor={'#69707F'}
                color={'#ED6E1E'}
              />
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.mdbottomView}
              onPress={() => {
                selectOption()
              }}>
              <Text style={styles.mdBottomText}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  const manuallyCodeFuncation = () => {
    return (
      <Modal
        visible={modalOpenManuallyCode}
        transparent={true}
        onRequestClose={() => {
          setModalOpenManuallyCode(false);
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <View style={styles.mdtop_1}>
              <Text style={styles.mdTopText}>Seal Code No</Text>
            </View>

            <TextLabel title={'Seal Code'} />
            <TextInput
              style={styles.textInput}
              placeholder={'Seal Code'}
              keyboardType={'phone-pad'}
              // maxLength={4}
              value={sealCode}
              placeholderTextColor={'#8D92A3'}
              onChangeText={text => { setSealCode(text) }}
            />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 20 }}>
              <TouchableOpacity
                style={styles.touchFuel}
                onPress={() => { setModalOpenManuallyCode(false) }}>
                <Text style={styles.touchtextFuel}>CANCEL</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.touchedFuel}
                onPress={() => { driverSealApi() }}>
                {isLoader ?
                  <ActivityIndicator size={'small'} color='#fff' />
                  :
                  <Text style={[styles.touchtextFuel, { color: '#ffffff' }]}>SAVE</Text>
                }
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  const deleteDriverFuncation = () => {
    return (
      <Modal
        visible={modalOpenDelete}
        transparent={true}
        onRequestClose={() => {
          setModalOpenDelete(false);
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <Text style={styles.textDelete}>Confirmation</Text>
            <Text style={styles.subtextDelete}>
              Do you want to delete this driver ?
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, paddingHorizontal: 20 }}>
              <TouchableOpacity
                style={styles.touchDelete}
                onPress={() => { setModalOpenDelete(false); }}>
                <Text style={styles.touchtextDelete}>CANCEL</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.touchedDetele}
                onPress={() => { driverDeleteApi() }}>
                <Text style={[styles.touchtextDelete, { color: '#ffffff' }]}>DELETE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  const driverList = (item, index) => {
    return (
      <View style={styles.subViewCardOffCss}>
        <View style={{ flexDirection: 'row', marginTop: 4 }}>
          <View style={{ flex: 0.8 }}>
            <Text style={styles.text}>Name : {item.name}</Text>
          </View>
          <View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => {
                setFuelAddID(item.id);
                setMobileNum(item.phone);
                setDriverName(item.name);
                setVechileId(item.user_info?.vehicle_number);
                setModalOpenUpdateFriver(true);
              }}>
              <Image
                style={styles.editImageOffCss}
                source={require('../images/vendor-app-slice/edit.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => { setModalOpenDelete(true), setFuelAddID(item.id) }}>
              <Image
                style={styles.deleteImageOffCss}
                source={require('../images/vendor-app-slice/delete.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <Text style={styles.subtext}>Vehicle Number : {item.user_info?.vehicle_number}</Text>
        <Text style={styles.subtext}>Mobile No : {item.phone}</Text>
        <Text style={[styles.subtext, { marginBottom: 10 }]}>Quantity : {item.quantity} LTR</Text>
        <DashLine margin={5} />
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 0, }}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.touch}
            onPress={() => { setModalOpenTopUp(true), setFuelAddID(item.id) }}>
            <Text style={styles.touchtext}>TOP UP</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.touched}
            onPress={() => {
              setModalOpenScanSeal(true); setFuelAddID(item.id)
            }}>
            <Text style={[styles.touchtext, { color: '#fff' }]}>SCAN SEAL</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  return (
    <View style={styles.containr}>
      {/* <StatusBarLight /> */}
      {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: '#31B9EB'
        }}>
          <StatusBar barStyle={"light-content"} translucent={true} {...navigation} />
        </View>
        :
        <StatusBarLight />
      }
      <Header onPress={() => navigation.goBack()} title="My Drivers" />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 20 }}>
        {isLoader ?
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator size={'large'} color='#000' />
          </View>
          :
          driversData.length > 0 ?
            driversData.map((item, index) => {
              return (
                driverList(item, index)
              )
            })
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 18, color: '#000', fontWeight: '700' }}>Not Available</Text>
            </View>
        }
      </ScrollView>
      {scanSealFunction()}
      {topUpFuncation()}
      {manuallyCodeFuncation()}
      {deleteDriverFuncation()}
      {upDateDriverDetails()}
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.touchButton}
        onPress={() => { navigation.navigate('CreateDriver') }}>
        <Text style={styles.textButton}>CREATE DRIVER</Text>
      </TouchableOpacity>
    </View>
  );
}
export default MyDrivers;
const TextLabel = ({ title }) => <Text style={styles.textLabel}>{title}</Text>;
const styles = StyleSheet.create({
  containr: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  subViewCardOffCss: {
    elevation: 5,
    borderRadius: 16,
    paddingVertical: 8,
    paddingHorizontal: 10,
    // marginVertical: 8,
    marginTop: 15,
    marginBottom: 5,
    backgroundColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    //   shadowRadius: 60,
    shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
  },
  text: {
    fontFamily: 'AVenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  editImageOffCss: {
    width: 17, height: 17, marginLeft: 15,
  },
  deleteImageOffCss: {
    width: 15,
    height: 18,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#83878E',
    marginTop: 5,
  },
  touch: {
    borderRadius: 5,
    borderWidth: 2,
    width: 90,
    height: 25,
    borderColor: '#ED6E1E',
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ED6E1E',
  },
  touched: {
    borderRadius: 5,
    width: 95,
    height: 25,
    marginLeft: 15,
    backgroundColor: '#ED6E1E',
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchButton: {
    backgroundColor: '#ED6E1E',
    alignItems: 'center',
    paddingVertical: 12,
    borderRadius: 25,
    marginVertical: Platform.OS === 'ios' ? 20 : 10,
    marginHorizontal: 20,
  },
  textButton: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    marginHorizontal: 100,
    color: '#FFFFFF',
  },
  modal_View: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#00000080',
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    // marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1: {
    backgroundColor: '#31B9EB',
    // padding: 10,
    paddingVertical: 15,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 20,
  },
  mdTopText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  textLabel: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#7A7A7A',
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 0,
  },
  textInput: {
    borderRadius: 8,
    backgroundColor: '#727C8E28',
    fontSize: 15,
    fontFamily: 'Aviner-Medium',
    fontFamily: '500',
    color: '#000',
    marginHorizontal: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  touchFuel: {
    borderRadius: 5,
    borderWidth: 1,
    height: 45,
    borderColor: '#E02020',
    width: "45%",
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchtextFuel: {
    fontFamily: 'Avenir-Heavy', fontSize: 15, fontWeight: 'bold', textAlign: 'center', color: '#E02020',
  },
  touchedFuel: {
    borderRadius: 5, height: 45, backgroundColor: '#ED6E1E', width: "45%",
    alignItems: 'center', justifyContent: 'center', marginBottom: 10
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 20,
    borderBottomWidth: 0.6,
    borderBottomColor: '#00000050',
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  mdbottomView: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    paddingVertical: 10,
    marginHorizontal: 25,
    marginVertical: 10,
    marginBottom: 20,
    marginTop: 20,
    alignItems: 'center'
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  textDelete: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginTop: 10,
    marginHorizontal: 20,
  },
  subtextDelete: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#6F6F7B',
    marginTop: 5,
    marginBottom: 20,
    marginHorizontal: 20,
  },
  touchDelete: {
    borderRadius: 5,
    height: 45,
    width: "45%",
    backgroundColor: '#FF0000',
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchtextDelete: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
  },
  touchedDetele: {
    borderRadius: 5,
    height: 45,
    width: "45%",
    backgroundColor: '#00C327',
    alignItems: 'center',
    justifyContent: 'center'
  },
  mdtop_1UpDate: {
    backgroundColor: '#31B9EB',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  mdTopTextUpDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 12,
  },
  mdbottomViewUpDate: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 25,
    marginBottom: 20,
  },
  mdBottomTextUpDate: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  textLabelUpDate: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#0E1236',
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 0,
  },
  textInputUpDate: {
    borderRadius: 10,
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginHorizontal: 20,
    marginTop: 5,
    marginBottom: 0,
    borderColor: '#727C8E80',
    fontSize: 12,
    fontFamily: 'Aviner-Medium',
    fontFamily: '500',
    color: '#000'
  },
});
