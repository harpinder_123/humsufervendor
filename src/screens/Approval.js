import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';

const {height, width} = Dimensions.get('window');
const Approval = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <TouchableOpacity
          style={styles.mdtop}
          onPress={() => navigation.navigate('DieselPrice')}>
          <Image source={require('../images/tick.png')} style={styles.image} />
          {/* <TouchableOpacity
            activeOpacity={0.8}
            style={styles.mdbottomView}
            onPress={() => {
              navigation.navigate('');
              setModalOpen(false);
            }}>
            <Text style={styles.mdBottomText}>SUBMIT</Text>
          </TouchableOpacity> */}
          <Text style={styles.text}>Registration submitted for approval.</Text>
          <Text style={styles.subtext}>
            After approval from admin you can {`\n`} use the app.
          </Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export default Approval;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#1E1F20',
    textAlign: 'center',
    marginTop: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#6F6F7B',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
});
