import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View, Modal, StatusBar, TurboModuleRegistry
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextField } from 'react-native-material-textfield';
import { BottomButton } from '../Custom/CustomView';
import { Api, LocalStorage } from '../services/Api';
import { _SetAuthToken } from '../services/ApiSauce';
import { useDispatch } from 'react-redux';
import * as actions from '../redux/actions';
import DeviceInfo from 'react-native-device-info';
import messaging from '@react-native-firebase/messaging';
import Toast from 'react-native-simple-toast';
import { validateEmail } from '../services/utils';
import { StatusBarLight } from '../Custom/CustomStatusbar';

const { height } = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <Image source={props.tfSource} style={[styles.tfImage, {
        width: props.width,
        height: props.height,
      }]} />
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        lineWidth={0.8}
        tintColor="#8D92A3"
        {...props}
      />
    </View>
  </>
);

const Login = ({ navigation }) => {
  const [state, setState] = useState({
    emailphone: '', // 9811517757 rajautomobiles123@gmail.com
    password: '', // 123456 raj@123
    isLoding: false,
    setApple_token: '',
    fcmToken: '',
    setValidation: false
  });

  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      DeviceInfo.getDeviceToken().then((deviceToken) => {
        setState({ ...state, setApple_token: deviceToken })
        console.log('-----deviceToken Login : ', deviceToken);
      });
      requestUserPermission();
    }, 1000);
    const unsubscribe = navigation.addListener('focus', () => {
      requestUserPermission();
    });
    return unsubscribe;
  }, [navigation])

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      getFcmToken();
    }
  }

  const getFcmToken = async () => {
    try {
      let fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('+++++++fcmToken:  ' + JSON.stringify(fcmToken));
        LocalStorage.setFcmToken(fcmToken)
        setState({ ...state, fcmToken: fcmToken })
      }
    } catch (error) {
      console.log(error, '-------------error');
    }
  };


  const onSubmitHandler = async () => {

    const { emailphone = '', password = '', fcmToken, setApple_token } = state;

    if (emailphone === '') {
      setState({ ...state, setValidation: true });
      return;
    }
    if (!validateEmail(emailphone)) {
      // setState({ ...state, setValidation: true });
      Toast.show('Please enter valid eamil')
      return;
    }
    if (password === '') {
      setState({ ...state, setValidation: true });
      return;
    }
    const body = {
      login_type: 'app',
      emailphone: emailphone,
      password: password,
      devicetoken: fcmToken,
      device_type: Platform.OS === 'ios' ? 'IOS' : "Android",
      apple_token: setApple_token,
    };
    console.log('-----body Login : ', body);
    setState({ ...state, isLoding: true });
    const response = await Api.signin(body);

    const {
      status = false,
      token = '',
      user_detail = {},
      signup_skip = false,
      msg
    } = response;
    if (status) {
      setState({ ...state, isLoding: false });
      LocalStorage.setToken(token);
      _SetAuthToken(token);
      LocalStorage.setUserDetail(JSON.stringify(response));
      dispatch(actions.SetUserDetail(user_detail));
      // console.log('-------resgition: ', response)
      const response2 = await Api.fuelUpdatePriceApi();
      const { status2 = false, change_flag } = response2;
      if (change_flag === 1) {
        navigation.replace('UpdateFuelPrice', { type: 'login' })
      } else {
        navigation.replace('DrawerNavigator')
      }
    } else {
      setState({ ...state, isLoding: false });
      const {
        data: { msg },
      } = response;
      if (msg == 'Your account is not verified. Please contact our customer support for more information.') {
        navigation.reset({
          index: 0,
          routes: [
            { name: 'RegistApproval' },
          ],
        })
      } else {
        Toast.show(msg)
        console.log('--', msg);
      }
    }
  };

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{ flex: 1, justifyContent: 'flex-end' }}>
      {/* {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: '#31B9EB'
        }}>
          <StatusBar barStyle={"light-content"} translucent={true} backgroundColor={'red'} {...navigation} />
        </View>
        : */}
      <StatusBarLight />
      {/* } */}

      <Image source={require('../images/background.png')} style={styles.bg} />
      <View style={styles.BottomView}>
        <Text style={styles.topText}>Login</Text>
        <Text style={styles.topText_1}>We will send you confirmation code</Text>

        <CustomInputField
          tfSource={require('../images/phone.png')}
          label="Email/Mobile"
          width={16}
          height={16}
          returnKeyType="next"
          defaultValue={state.emailphone}
          useNativeDriver={true}
          onChangeText={emailphone => setState({ ...state, emailphone })}
        />
        {state.setValidation == true && state.emailphone === '' ?
          <Text style={styles.validationStyle}>Enter valid email or phone number</Text>
          : <></>
        }
        <CustomInputField
          tfSource={require('../images/lock.png')}
          label="Password"
          width={14}
          height={16}
          returnKeyType="done"
          defaultValue={state.password}
          useNativeDriver={true}
          secureTextEntry={true}
          onChangeText={password => setState({ ...state, password })}
        />
        {state.setValidation == true && state.password === '' ?
          <Text style={styles.validationStyle}>Enter your password</Text>
          : <></>
        }
        {/* <TouchableOpacity onPress={() => navigation.navigate('ForgetPassword')}>
          <Text style={styles.bottomtext}>Forgot Password</Text>
        </TouchableOpacity> */}
        <View style={{ marginTop: 25, }}>
          <BottomButton loader={state.isLoding} bottomtitle="SIGN IN" onPress={() => { onSubmitHandler() }} />
        </View>
        <TouchableOpacity activeOpacity={0.7} onPress={() => { navigation.navigate('Register') }}>
          <Text style={styles.bottomtext1}>
            Don’t have an account?
            <Text style={{ color: '#ED6E1E', fontWeight: '700' }}> Signup Now</Text>
          </Text>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
  );
};
export default Login;

const styles = StyleSheet.create({
  bg: {
    flex: 1,
    position: 'absolute',
    width: "100%",
    height: "85%",
    bottom: "15%",
    resizeMode: 'cover'
  },
  BottomView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    paddingTop: 40,
    borderTopEndRadius: 50,
    borderTopStartRadius: 50,
  },
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 15,
    fontWeight: '400',
    color: '#8D92A3',
    marginHorizontal: 10,
    marginTop: 4,
  },
  tfStyle: {
    // marginLeft: 30,
    width: '85%',
    marginHorizontal: 10,
  },
  tfImage: {
    resizeMode: 'contain',
    marginTop: 15,
    marginLeft: 10,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    width: '95%',
    borderColor: '#F1F3F8',
    borderWidth: 0.5,
    marginHorizontal: 10,
    // borderColor:''
  },
  bottomtext: {
    fontFamily: 'Avenir-Roman',
    fontSize: 12,
    fontWeight: '400',
    color: '#8D92A3',
    marginTop: 10,
    alignSelf: 'flex-end',
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#8D92A3',
  },
  bottomtext1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
    marginTop: 5,
    alignSelf: 'center',
    marginBottom: 10,
  },
  validationStyle: {
    fontFamily: 'Avenir-Medium', fontSize: 14, fontWeight: '500', color: '#FA7272', marginHorizontal: 35
  },
});
