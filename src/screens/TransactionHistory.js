import React, { useState } from 'react';
import {
    FlatList,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import { Header } from '../Custom/CustomView';
import { Api, LocalStorage } from '../services/Api';
import moment from 'moment';
import Toast from 'react-native-simple-toast';

const SCREEN_HEIGHT = Dimensions.get('screen').height

const TransactionHistory = ({ navigation, route }) => {
    const [userData, setUserData] = useState('')
    const [todayBalance, setTodayBalance] = useState(0);
    const [totalFuel, setTotalFuel] = useState(0);
    const [checked, setChecked] = useState(0);
    const [conditionValue, setConditionValue] = useState('today')
    const [modalVisible, setModalVisible] = useState(false);
    const [isLoader, setIsLoader] = useState(true);
    const [modalFlatListData, setModalFlatListData] = useState([
        { title: 'Today Transaction', smileTitle: 'today' },
        { title: 'Tomorrow Transaction', smileTitle: 'yesterday' },
        { title: 'Date Range', smileTitle: 'daterange' },
    ]);
    const [transactionHistoryData, setTransactionHistoryData] = useState([])

    React.useEffect(() => {
        transactionHistoryApi(conditionValue)
        LocalStorage.getUserDetail('userdata').then((userdata) => {
            setUserData(JSON.parse(userdata))
            getProfileApi()
        })
    }, [])

    const getProfileApi = async () => {
        const response = await Api.getProfile();
        const { status = false, vendor_info = [] } = response;
        if (vendor_info[0].status === 0) {
            navigation.replace('Login');
        }
    }

    const transactionHistoryApi = async (conditions) => {
        const body = {
            driver_id: 97,
            condition: conditions,
            specific_date: '',
            specific_date_end: '',
        };
        const response = await Api.transactionHistory(body);
        const { status = false } = response;
        if (status) {
            setIsLoader(false);
            setTodayBalance(response.today_balance);
            setTotalFuel(response.total_fuel);
            setTransactionHistoryData(response.list)
        } else {
            setIsLoader(false);
            Toast.show('Something went wrong')
        }
    }

    const transactionList = (item, index) => {
        moment.locale('en');
        return (
            <View style={styles.nlBg}>
                <View style={styles.topList}>
                    <View style={styles.line} />
                    <View style={{ flex: 1, paddingHorizontal: 12, paddingVertical: 7 }}>
                        <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }}>
                            <Text style={styles.listText}>{item.collectUser}</Text>
                            <Text style={[styles.listText, { color: item.trans_type === 'credit' ? '#5DC462' : '#FF0000', }]}>{item.after_quantity}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }}>
                            <Text style={styles.listText_1}>{moment(item.created_at).format("DD MMMM, h:mm A")}</Text>
                            <Text style={styles.listText_1}>ID: {item.order_id}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    const openCalendarModal = (visible) => {
        setModalVisible(visible)
    }

    const selectJob = (item, index) => {
        console.log('---item: ', item.smileTitle)
        setConditionValue(item.smileTitle)
        setChecked(index)
        setTimeout(() => {
            transactionHistoryApi(item.smileTitle)
            openCalendarModal(false)

        }, 200);
    }

    const transactionHistoryCalendar = () => {
        return (
            <Modal
                transparent={true}
                visible={modalVisible}
                // statusBarTranslucent={true}
                onRequestClose={() => { openCalendarModal(false) }}>
                <View style={styles.modalViewOffCss}>
                    <View style={styles.modalCardViewOffCss}>
                        <FlatList
                            data={modalFlatListData}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => { selectJob(item, index) }}
                                        style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 8 }}>
                                        <Text style={{ color: '#000', fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 14 }}>{item.title}</Text>
                                        <Image
                                            style={{ width: 18, height: 18, resizeMode: 'contain' }}
                                            source={checked == index ? require('../images/radio.png') : require('../images/radioUnfilll.png')} />
                                    </TouchableOpacity>
                                )
                            }}
                        />
                    </View>
                </View>
            </Modal>
        )
    }

    return (
        <View styles={styles.bg}>
            <Header onPress={() => navigation.goBack()} title={'Driver Detail'} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <Text style={styles.text}>Today Balance : ₹ {todayBalance}</Text>
                    <Text style={styles.subtext}>Total Fuel : {totalFuel} ltr</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20 }}>
                    <Text style={styles.texted}>Transaction History</Text>
                    <TouchableOpacity onPress={() => { openCalendarModal(true) }}>
                        <Image
                            style={styles.image}
                            source={require('../images/calendar.png')}
                        />
                    </TouchableOpacity>
                </View>
                {transactionHistoryCalendar()}
                {isLoader ?
                    <View style={styles.loaderViewOff}>
                        <ActivityIndicator size={'large'} color={'#000'} />
                    </View>
                    : transactionHistoryData.length > 0 ?
                        transactionHistoryData.map((item, index) => {
                            return (
                                transactionList(item, index)
                            )
                        })
                        :
                        <View style={styles.loaderViewOff}>
                            <Text style={styles.notFoundTextOff}>Not Available</Text>
                        </View>
                }
            </ScrollView>
        </View>
    );
};

export default TransactionHistory;

const styles = StyleSheet.create({
    bg: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    nlBg: {
        backgroundColor: '#FFFFFF',
        marginVertical: 10,
        marginHorizontal: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 5,
        marginBottom: 10,
    },
    line: {
        width: '2%',
        backgroundColor: '#ED6E1E',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    topList: {
        flexDirection: 'row',
    },
    listImage: {
        width: 40,
        height: 40,
        resizeMode: 'contain',
        margin: 10,
    },
    listText: {
        fontFamily: 'Avenir-Medium',
        fontSize: 14,
        fontWeight: '500',
        color: '#272727',
    },
    listText_1: {
        fontFamily: 'Avenir-Medium',
        fontSize: 12,
        fontWeight: '500',
        color: '#83878E',
    },
    container: {
        padding: 15,
        marginHorizontal: 20,
        backgroundColor: '#31B9EB',
        borderRadius: 20,
        marginTop: 20,
    },
    text: {
        fontFamily: 'Avenir-Heavy',
        fontSize: 22,
        fontWeight: '900',
        color: '#F7F7F7',
        marginHorizontal: 10,
    },
    subtext: {
        fontFamily: 'Avenir-Heavy',
        fontSize: 20,
        fontWeight: '900',
        color: '#F7F7F7',
        marginHorizontal: 10,
        marginTop: 5,
        marginBottom: 10,
    },
    texted: {
        fontFamily: 'Avenir-Medium',
        fontSize: 18,
        fontWeight: '500',
        color: '#272727',
        marginTop: 20,
    },
    image: {
        height: 25,
        width: 25,
        marginTop: 20,
        marginRight: 15,
    },
    modalViewOffCss: {
        backgroundColor: '#00000030', flex: 1, alignItems: 'center'
    },
    modalCardViewOffCss: {
        backgroundColor: '#fff', width: 220, height: 118, borderRadius: 5,
        top: "31%", left: "10%", paddingHorizontal: 15, paddingVertical: 5, elevation: 5
    },
    loaderViewOff: {
        height: SCREEN_HEIGHT / 1.4, justifyContent: 'center', alignItems: 'center'
    },
    notFoundTextOff: {
        fontSize: 16, fontWeight: '500', color: '#000'
    }
});
