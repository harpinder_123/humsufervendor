import React, { useState } from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { StatusBarDark } from '../Custom/CustomStatusBar';
import { Header, MainView } from '../Custom/CustomView';
import { RadioButton } from 'react-native-paper';
import { FlatList } from 'react-native-gesture-handler';

const { height, width } = Dimensions.get('window');
const Reject = ({ navigation }) => {
  const [modalOpen, setModalOpen] = useState(true);
  const [checked, setChecked] = useState(null);
  const [modalData, setModalData] = useState([
    { title: 'Other Reason' }, { title: 'Other Reason' }, { title: 'Other Reason' },
    { title: 'Other Reason' }, { title: 'Other Reason' },
  ])

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <View style={styles.topView}>
            <Text style={styles.text1_Style}>Reject Reason:</Text>
            <FlatList
              data={modalData}
              renderItem={({ item, index }) => {
                return (
                  <TouchableOpacity
                    style={styles.topv4_Style}
                    onPress={() => setChecked(index)}>
                    <RadioButton
                      value="first"
                      status={checked === index ? 'checked' : 'unchecked'}
                      onPress={() => setChecked(index)}
                      uncheckedColor={'#69707F'}
                      color={'#ED6E1E'}
                    />
                    <Text style={styles.text2_Style}>{item.title}</Text>
                  </TouchableOpacity>
                )
              }}
            />
          </View>
          <View style={{ flexDirection: 'row', }}>
            <View style={{ width: '45%' }}>
              <TouchableOpacity
                activeOpacity={0.8}
                // style={styles.mdbottomView}
                onPress={() => {
                  navigation.navigate('NewOrder');
                  setModalOpen(false);
                }}>
                <Text style={styles.mdBottomText}>DISMISS</Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: '45%' }}>
              <TouchableOpacity
                activeOpacity={0.8}
                // style={styles.mdbottomView}
                onPress={() => {
                  navigation.navigate('NewOrder');
                  setModalOpen(false);
                }}>
                <Text style={styles.sdBottomText}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default Reject;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#E02020',
    textAlign: 'center',
    marginBottom: 20,
  },
  sdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#ED6E1E',
    textAlign: 'center',
    marginBottom: 20,
  },
  topView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: 15,
    paddingBottom: 20,
  },
  text1_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    color: '#080040',
    fontWeight: 'bold',
    marginHorizontal: 20,
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 10,
  },
  text2_Style: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#6E7584',
    fontWeight: '500',
  },
});
