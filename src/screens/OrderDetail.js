import React, { useEffect, useState } from 'react';
import { View, Text, ScrollView, StyleSheet, Platform, StatusBar, Linking } from 'react-native';
import Dash from 'react-native-dash';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { Header } from '../Custom/CustomView';
import { Api } from '../services/Api';
import moment from 'moment';

const OrderDetail = ({ navigation, route }) => {
    const [orderDetail, setOrderDetail] = useState(route.params?.cardDetails)

    useEffect(() => {
        getProfileApi()
    }, [navigation])

    const getProfileApi = async () => {
        const response = await Api.getProfile();
        const { status = false, vendor_info = [] } = response;
        if (vendor_info[0].status === 0) {
            navigation.replace('Login');
        }
    }

    moment.locale('en');
    return (
        <View style={styles.containr}>
            {Platform.OS === 'ios' ?
                <View style={{
                    width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
                    backgroundColor: '#31B9EB'
                }}>
                    <StatusBar barStyle={"light-content"} translucent={true} {...navigation} />
                </View>
                :
                <StatusBarLight />
            }
            <Header onPress={() => navigation.goBack()} title="Order Detail" />
            <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                <View style={{ marginVertical: 20 }}>
                    <View style={styles.textViewOffCss}>
                        <Text style={styles.textOffCss}>Name</Text>
                        <Text style={styles.nameTextOffCss}>{orderDetail.order_by_detail?.name}</Text>
                    </View>
                    <View style={styles.textViewOffCss}>
                        <Text style={styles.textOffCss}>Mobile No.</Text>
                        <Text
                            onPress={() => {
                                Linking.openURL('tel:' + orderDetail.order_by_detail?.phone);
                            }}
                            style={styles.nameTextOffCss}>{orderDetail.order_by_detail?.phone}</Text>
                    </View>
                    <View style={styles.textViewOffCss}>
                        <Text style={styles.textOffCss}>Delivery Address</Text>
                        <Text style={styles.nameTextOffCss}>{orderDetail.delivery_address_detail?.address}</Text>
                    </View>
                    {orderDetail.driver_details?.name &&
                        <View style={styles.textViewOffCss}>
                            <Text style={styles.textOffCss}>Driver Name</Text>
                            <Text style={styles.nameTextOffCss}>{orderDetail.driver_details?.name}</Text>
                        </View>
                    }
                    {orderDetail.driver_details?.driver_info?.vehicle_number &&
                        <View style={styles.textViewOffCss}>
                            <Text style={styles.textOffCss}>Truck Number</Text>
                            <Text style={styles.nameTextOffCss}>{orderDetail.driver_details?.driver_info?.vehicle_number}</Text>
                        </View>
                    }
                    <View style={styles.textViewOffCss}>
                        <Text style={styles.textOffCss}>Quantity (in Litres)</Text>
                        <Text style={styles.nameTextOffCss}>{orderDetail.quantity} LTR</Text>
                    </View>
                    <View style={styles.textViewOffCss}>
                        <Text style={styles.textOffCss}>Date & Time</Text>
                        <Text style={styles.nameTextOffCss}>{moment(orderDetail.delivery_date, "YYYY-MM-DD, h:mm a").format("DD MMMM YY, h:mm A")}</Text>
                        {/* <Text style={styles.nameTextOffCss}>{orderDetail.delivery_date}</Text> */}
                    </View>
                    {orderDetail.reason != null &&
                        <View style={styles.textViewOffCss}>
                            <Text style={styles.textOffCss}>Reason</Text>
                            <Text style={styles.nameTextOffCss}>{orderDetail.reason} </Text>
                        </View>
                    }
                    <View style={styles.textViewOffCss}>
                        <Text style={styles.textOffCss}>Order Id</Text>
                        <Text style={styles.nameTextOffCss}>#{orderDetail.id}</Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};
export default OrderDetail;
const styles = StyleSheet.create({
    containr: {
        flex: 1,
        backgroundColor: '#FAFAFA',
    },
    textViewOffCss: {
        backgroundColor: '#FFF', paddingHorizontal: 15, paddingVertical: 10,
        borderBottomWidth: 0.6, borderBottomColor: '#C8C8D3'
    },
    textOffCss: {
        fontSize: 14, fontWeight: '500', color: '#83878E'
    },
    nameTextOffCss: {
        fontSize: 16, fontFamily: 'Avenir-Heavy', fontWeight: '700', color: '#000521',
        paddingVertical: 8
    }
});
