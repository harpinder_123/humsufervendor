import React, { useState, useEffect, useRef } from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View, ScrollView, ActivityIndicator
} from 'react-native';
import { StatusBarLight, StatusBarDark } from '../Custom/CustomStatusbar';
import { Api } from '../services/Api';
import Toast from 'react-native-simple-toast';

const { height, width } = Dimensions.get('window');

const UpdateDriver = ({ navigation, route }) => {
  const [driverItem, setdriverItem] = useState(route.params?.driverId);
  const [mobileNum, setMobileNum] = useState(driverItem.phone);
  const [driverName, setDriverName] = useState(driverItem.name);
  const [vechileId, setVechileId] = useState(driverItem.user_info?.vehicle_number);
  const [modalOpenUpdateFriver, setModalOpenUpdateFriver] = useState(true);
  const [isLoader, setIsLoader] = useState(false);

  const input_driver = useRef(null)
  const input_number = useRef(null)
  const input_name = useRef(null)
  const input_vechile = useRef(null)

  const upDateProfileDravers = async () => {
    if (!mobileNum) {
      Toast.show('Please enter your Mobile Number');
      return false;
    }
    if (mobileNum.length < 10) {
      Toast.show('Please enter your Mobile Number');
      return false;
    }
    if (!driverName) {
      Toast.show('Please enter your Dariver Name');
      return false;
    }
    if (!vechileId) {
      Toast.show('Please enter your Vechile Id');
      return false;
    }
    const body = {
      driver_id: driverItem.id,
      mobile: mobileNum,
      name: driverName,
      car_id: vechileId
    };
    setIsLoader(true);
    const response = await Api.driverProfileUpdateApi(body);
    const { status = false, msg } = response;
    if (status) {
      setModalOpenUpdateFriver(false);
      setIsLoader(false);
      Toast.show(msg);
      navigation.navigate('MyDrivers');
    } else {
      Toast.show(msg);
      setIsLoader(false);
      setModalOpenUpdateFriver(false);
    }
  }

  return (
    <Modal
      visible={modalOpenUpdateFriver}
      transparent={true}
      onRequestClose={() => {
        setModalOpenUpdateFriver(false);
        navigation.navigate('MyDrivers');
      }}>
      <ScrollView style={styles.modal_View}>
        <StatusBarDark />
        <View style={styles.mdtop}>
          <View style={styles.mdtop_1UpDate}>
            <Text style={styles.mdTopTextUpDate}>Update Driver</Text>
          </View>
          <Text style={styles.textLabelUpDate}>Driver ID</Text>
          <View style={[styles.textInputUpDate, { paddingVertical: 10, }]}>
            <Text style={{ color: '#000', fontSize: 12, fontWeight: '500' }}>{driverItem.id}</Text>
          </View>
          {/* <TextInput
            style={styles.textInput}
            placeholder={'Driver ID'}
            keyboardType={'phone-pad'}
            returnKeyType="next"
            value={driverId}
            editable={false}
            onChangeText={(text) => { setDriverId(text) }}
            onSubmitEditing={() => input_number.current.focus()}
          /> */}

          <Text style={styles.textLabelUpDate}>Mobile Number</Text>
          <TextInput
            style={styles.textInputUpDate}
            placeholder={'Mobile Number'}
            keyboardType={'phone-pad'}
            returnKeyType="next"
            maxLength={10}
            value={mobileNum}
            onChangeText={(text) => { setMobileNum(text) }}
            onSubmitEditing={() => input_number.current.focus()}
            ref={input_name}
          />

          <Text style={styles.textLabelUpDate}>Name</Text>
          <TextInput
            style={styles.textInputUpDate}
            placeholder={'Name'}
            returnKeyType="next"
            value={driverName}
            onChangeText={(text) => { setDriverName(text) }}
            onSubmitEditing={() => input_vechile.current.focus()}
            ref={input_number}
          />

          <Text style={styles.textLabelUpDate}>Vechile</Text>
          <TextInput
            style={styles.textInputUpDate}
            placeholder={'Vechile ID'}
            keyboardType={'phone-pad'}
            returnKeyType="done"
            value={vechileId}
            onChangeText={(text) => { setVechileId(text) }}
            // onSubmitEditing={() => input_number.current.focus()}
            ref={input_vechile}
          />
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.mdbottomViewUpDate}
            onPress={() => { upDateProfileDravers() }}>
            {isLoader ?
              <ActivityIndicator size={'small'} color='#fff' />
              :
              <Text style={styles.mdBottomTextUpDate}>UPDATE</Text>
            }
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Modal>
  );
};

export default UpdateDriver;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#00000030',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1UpDate: {
    backgroundColor: '#31B9EB',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  mdTopTextUpDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginHorizontal: 12,
  },
  mdbottomViewUpDate: {
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    marginHorizontal: 25,
    marginBottom: 20,
  },
  mdBottomTextUpDate: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  textLabelUpDate: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#0E1236',
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 0,
  },
  textInputUpDate: {
    borderRadius: 10,
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginHorizontal: 20,
    marginTop: 5,
    marginBottom: 0,
    borderColor: '#727C8E80',
    fontSize: 12,
    fontFamily: 'Aviner-Medium',
    fontFamily: '500',
    color: '#000'
  },
});
