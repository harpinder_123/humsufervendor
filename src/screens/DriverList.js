import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { Header } from '../Custom/CustomView';
import Loader from '../Custom/Loader';
import { Api, LocalStorage } from '../services/Api';
import { _SetAuthToken, _RemoveAuthToken } from '../services/ApiSauce';
import * as actions from '../redux/actions';
import { ActivityIndicator } from 'react-native-paper';

const MyDrivers = ({ navigation }) => {
  const [driverState, setDriverState] = useState([]);
  const [isLoader, setIsLoader] = useState(true)

  useEffect(() => {
    navigationHandler();
    getProfileApi()
  }, []);

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }

  const navigationHandler = async () => {
    const response = await Api.driverList();
    const { status = false, drivers = [] } = response;
    setTimeout(() => {
      setIsLoader(false)
      setDriverState(drivers);
    }, 500);
  };
  return (
    <View style={{ flex: 1, }}>
      <StatusBarLight />
      <Header onPress={() => navigation.goBack()} title="Driver List" />
      <View style={{ flex: 1, paddingVertical: 10, justifyContent: 'center' }}>
        {isLoader ?
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator size={'small'} color='#000' />
          </View>
          : <FlatList
            data={driverState}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={styles.driverViewOffCss}
                  onPress={() => navigation.navigate('DriverDetails', { driverId: item.id })}>
                  <Text style={styles.driverTextOffCss}>{item.name}</Text>
                  <Text style={styles.subtext}>
                    {item.user_info.vehicle_number}
                  </Text>
                </TouchableOpacity>
              );
            }}
          />
        }

      </View>
    </View>
  );
};
export default MyDrivers;
const styles = StyleSheet.create({
  driverViewOffCss: {
    backgroundColor: '#ffffff',
    // padding: 20,
    elevation: 5,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginHorizontal: 20,
    marginVertical: 8,
    borderRadius: 5,
  },
  driverTextOffCss: {
    fontFamily: 'AVenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#8F92A1',
    marginTop: 5,
  },
});
