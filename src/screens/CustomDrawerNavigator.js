import { DrawerContentScrollView } from '@react-navigation/drawer';
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  View,
  Alert,
} from 'react-native';
import * as actions from '../redux/actions';
import { Api, height, LocalStorage } from '../services/Api';
import { _RemoveAuthToken } from '../services/ApiSauce';
const CustomDrawerContent = ({ navigation }) => {
  const [userData, setUserData] = useState("");
  const [privacyTermsCondition, setPrivacyTermsCondition] = useState("");
  const Data = [
    {
      id: 0,
      title: 'Home',
      value: 'Home',
      source: require('../images/menu_home.png'),
    },
    {
      id: 1,
      title: 'My Drivers',
      value: 'MyDrivers',
      source: require('../images/vendor-app-slice/menu_driver.png'),
    },
    {
      id: 2,
      title: 'Driver List',
      value: 'DriverList',
      source: require('../images/vendor-app-slice/menu_driver.png'),
    },
    {
      id: 3,
      title: 'Update Fuel',
      value: 'UpdateFuel',
      source: require('../images/vendor-app-slice/company.png'),
    },
    {
      id: 4,
      title: 'Transaction History',
      value: 'TransactionHistory',
      source: require('../images/vendor-app-slice/menu_history.png'),
    },

    {
      id: 5,
      title: 'Support',
      value: 'Support',
      source: require('../images/vendor-app-slice/menu_support.png'),
    },
    {
      id: 6,
      title: 'Privacy Policy',
      value: 'PrivacyPolicy',
      source: require('../images/vendor-app-slice/menu_terms.png'),
    },
    {
      id: 7,
      title: 'Terms & Condition',
      value: 'Terms&Condition',
      source: require('../images/vendor-app-slice/menu_password.png'),
    },
    {
      id: 8,
      title: 'Logout',
      value: 'Logout',
      source: require('../images/menu_logout.png'),
    },
  ];

  useEffect(() => {
    LocalStorage.getUserDetail('userdata').then((userdata) => {
      setUserData(JSON.parse(userdata))
      setTimeout(() => {
        driverSealApi()
      }, 2000);
    })
  }, [])

  const driverSealApi = async () => {
    const response = await Api.mastersettingsApi();
    const { status = false, msg, data = {} } = response;
    if (status) {
      setPrivacyTermsCondition(data)
    } else {
      // alert(msg);
      // setIsLoader(false);
    }
  }

  const onLogoutHandler = () => {
    _RemoveAuthToken();
    LocalStorage.setToken('');
    // dispatch(actions.SetLogout());
    LocalStorage.clear()
    navigation.closeDrawer,
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }],
      });
  };

  const gotoWebViewScreen = (title) => {
    let url = "";
    if (title === "Privacy Policy") {
      url = privacyTermsCondition.privacy
    } else if (title === "Terms & Condition") {
      url = privacyTermsCondition.terms
    }
    navigation.navigate('WebViewScreen', {
      title: title,
      url: url
    })
  }

  const onPressHandler = value => {
    navigation.closeDrawer();
    switch (value) {
      case 'Home':
        navigation.navigate('Home');
        break;
      case 'MyDrivers':
        navigation.navigate('MyDrivers');
        break;
      case 'DriverList':
        navigation.navigate('DriverList');
        break;
      case 'UpdateFuel':
        navigation.navigate('UpdateFuelPrice');
        break;
      case 'TransactionHistory':
        navigation.navigate('TransactionHistoryVander');
        break;
      case 'SavedAddress':
        navigation.navigate('SavedAddress');
        break;
      case 'Support':
        navigation.navigate('Support');
        break;
      case 'MyWallet':
        navigation.navigate('TopNavigation');
        break;
      case 'Refer&Earn':
        navigation.navigate('BloodDonationHistory');
        break;
      case 'Report':
        navigation.navigate('DonationHistory');
        break;
      case 'Help':
        navigation.navigate('Help');
        break;
      case 'Faq':
        navigation.navigate('Faq');
        break;
      case 'PrivacyPolicy':
        // navigation.navigate('');
        gotoWebViewScreen('Privacy Policy')
        break;
      case 'Terms&Condition':
        gotoWebViewScreen('Terms & Condition')
        break;
      case 'Logout':
        Alert.alert(
          'Logout',
          `Do you want to logout.`,
          [
            {
              text: 'No',
              // onPress: navigation.closeDrawer,
              style: 'cancel',
            },
            { text: 'Yes', onPress: onLogoutHandler },
          ],
          { cancelable: false },
        );
        break;

      default:
    }
  };

  const optionView = ({ source, title, value }) => (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.controlView_2}
      onPress={() => onPressHandler(value)}>
      <Image source={source} style={styles.constrolViewImage} />
      <Text style={styles.controlViewText}>{title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#31B9EB' }}>
      <DrawerContentScrollView style={{ height: height / 1 }} showsVerticalScrollIndicator={false}>
        <View style={styles.controlView}>
          <Text style={styles.dnTopText}>{userData.user_detail?.name}</Text>
          <Text style={styles.dnTopText1}>+91 {userData.user_detail?.phone}</Text>
          <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
            <Text style={[styles.dnTopText1, { marginBottom: 10 }]}>
              View Profile
            </Text>
          </TouchableOpacity>
          {Data.map(item => (
            <>{optionView(item)}</>
          ))}
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Text style={styles.appVersion}>App version 3.0.0</Text>
        </View>
      </DrawerContentScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  controlView: {
    height: height / 1.060,
    backgroundColor: '#31B9EB',
    width: '100%',
    paddingTop: 10,
  },
  appVersion: {
    // fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#FFFFFF',
    opacity: 0.5,
  },
  controlView_2: {
    marginHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  constrolViewImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  controlViewText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    marginLeft: '4%',
  },
  dnTopText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#FFFFFF',
    marginTop: 20,
    marginHorizontal: 30,
  },
  dnTopText1: {
    // fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#FFFFFF',
    marginHorizontal: 30,
    marginTop: 5,
  },
});
export default CustomDrawerContent;
