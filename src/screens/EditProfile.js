import React, { useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View, Image, ActivityIndicator, Platform, StatusBar
} from 'react-native';
import { useStore } from 'react-redux';
import { StatusBarDark, StatusBarLight } from '../Custom/CustomStatusbar';
import { Header, MainView } from '../Custom/CustomView';
import Loader from '../Custom/Loader';
import { Api } from '../services/Api';
import * as actions from '../redux/actions';

const EditProfile = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingUpdate, setIsLoadingUpdate] = useState(false);
  const [state, setState] = useState({
    name: '',
    email: '',
    mobileNo: '',
    companyName: '',
    bankName: '',
    branchName: '',
    accountNo: '',
    ifscode: '',
    state: '',
    city: '',
  });

  React.useEffect(() => {
    onGetProfile()
  }, [])

  const onGetProfile = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    setIsLoading(false);
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    } else {
      setState({
        ...state,
        name: vendor_info[0].name,
        email: vendor_info[0].email,
        mobileNo: vendor_info[0].phone,
        state: vendor_info[0].state,
        city: vendor_info[0].city,
        companyName: vendor_info[0].company,
        bankName: vendor_info[0].user_info.bank_name,
        branchName: vendor_info[0].user_info.branch_name,
        accountNo: vendor_info[0].user_info.account_number,
        ifscode: vendor_info[0].user_info.ifsc_code,
      });
    }
  }

  const onSaveHandler = async () => {
    const { name, companyName, bankName, branchName, accountNo, ifscode } = state;
    const body = {
      name: name,
      company: companyName,
      bank_name: bankName,
      branch_name: branchName,
      account_number: accountNo,
      ifsc_code: ifscode,
    };
    setIsLoadingUpdate(true);
    const response = await Api.profileUpdate(body);
    const { status = false, msg, user_detail = {} } = response;
    if (status) {
      navigation.goBack()
      Toast.show(msg);
      setIsLoadingUpdate(false);
    } else {
      const {
        data: { msg = 'Something went wrong' },
      } = response;
      Toast.show(msg);
      setIsLoadingUpdate(false);
    }
  };

  return (
    <MainView>
      {/* <StatusBarDark /> */}
      {Platform.OS === 'ios' ?
        <View style={{
          width: "100%", position: 'absolute', top: 0, left: 0, height: 50,
          backgroundColor: '#31B9EB'
        }}>
          <StatusBar barStyle={"light-content"} translucent={true} {...navigation} />
        </View>
        :
        <StatusBarLight />
      }
      <Header onPress={() => navigation.goBack()} title="Edit Profile" height={Platform.OS === 'ios' ? 40 : 85} />
      {/* <Loader status={state.isLoading} /> */}
      {isLoading ?
        <View style={styles.laderView}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
        :
        <ScrollView
          contentContainerStyle={{ flexGrow: 1, paddingTop: 15 }}
          showsVerticalScrollIndicator={false}>
          <View style={{ flex: 1, marginBottom: "10%" }}>
            <TextLabel title={'Name'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  value={state.name}
                  onChangeText={name => setState({ ...state, name })}
                  style={styles.textInput}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
            <TextLabel title={'Email'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  editable={false}
                  value={state.email}
                  style={styles.textInput}
                  onChangeText={email => setState({ ...state, email })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
            <TextLabel title={'Mobile No.'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  editable={false}
                  value={state.mobileNo}
                  keyboardType='numeric'
                  style={styles.textInput}
                  onChangeText={phone => setState({ ...state, phone })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
            <TextLabel title={'Company'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  value={state.companyName}
                  style={styles.textInput}
                  onChangeText={text => setState({ ...state, companyName: text })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>

            <TextLabel title={'Bank Name'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  value={state.bankName}
                  style={styles.textInput}
                  onChangeText={bankName => setState({ ...state, bankName })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
            <TextLabel title={'Branch Name'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  value={state.branchName}
                  style={styles.textInput}
                  onChangeText={branchName => setState({ ...state, branchName })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
            <TextLabel title={'Account No.'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  value={state.accountNo}
                  style={styles.textInput}
                  keyboardType='numeric'
                  onChangeText={accountNo => setState({ ...state, accountNo })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
            <TextLabel title={'IFSC Code'} />
            <View style={styles.textInputView}>
              <View style={{ flex: 0.9 }}>
                <TextInput
                  value={state.ifscode}
                  style={styles.textInput}
                  // keyboardType='decimal-pad'
                  onChangeText={ifscode => setState({ ...state, ifscode })}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={{ flex: 0.1, alignItems: 'center' }}>
                <Image style={{ width: 15, height: 15, resizeMode: 'cover' }} source={require('../images/edit.png')} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ marginBottom: "5%", justifyContent: 'center' }}>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.touchButton}
              onPress={() => { onSaveHandler() }}>
              {isLoadingUpdate ?
                <View>
                  <ActivityIndicator size={'small'} color={'#fff'} />
                </View>
                :
                <Text style={styles.textButton}>UPDATE</Text>
              }
            </TouchableOpacity>
          </View>
        </ScrollView>
      }

    </MainView>
  );
};

export default EditProfile;
const TextLabel = ({ title }) => <Text style={styles.textLabel}>{title}</Text>;
const styles = StyleSheet.create({
  laderView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000050',
    zIndex: 10,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textLabel: {
    // fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#69707F',
    marginHorizontal: 20,
    // marginTop: 5,
    marginBottom: 0,
  },
  textInputView: {
    marginHorizontal: 20,
    paddingVertical: Platform.OS === 'ios' ? 12 : 0,
    marginBottom: 10,
    borderBottomColor: '#727C8E80',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    fontSize: 16,
    color: '#000000',
    fontFamily: 'Aviner-Medium',
    fontWeight: '500',
    flex: 1,
  },
  touchButton: {
    height: 45,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ED6E1E',
    marginHorizontal: 15,
  },
  textButton: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    letterSpacing: 1.5,
    color: '#FFFFFF',
  },
});
