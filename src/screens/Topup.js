import React, { useState } from 'react';
import {
  Dimensions, Modal, StyleSheet, Text, TouchableOpacity, View, Keyboard, TextInput, ActivityIndicator
} from 'react-native';
import { Api } from '../services/Api';
import Toast from 'react-native-simple-toast';

const { height, width } = Dimensions.get('window');

const Topup = ({ navigation, route }) => {
  const [driver_Id, setDriver_Id] = useState(route.params?.driverId)
  const [quantity, setQuantity] = useState('');
  const [modalOpen, setModalOpen] = useState(true);
  const [isLoader, setIsLoader] = useState(false)

  const upDateDriverQuantiApi = async () => {
    if (!quantity) {
      Toast.show('Please enter your quanitity')
      return false;
    }
    Keyboard.dismiss()
    const body = {
      driver_id: driver_Id,
      quantity: quantity
    };
    setIsLoader(true);
    const response = await Api.driverQuantityUpdateApi(body);
    const { status = false, msg } = response;
    if (status) {
      setModalOpen(false);
      setIsLoader(false);
      Toast.show(msg)
      navigation.navigate('MyDrivers')
    } else {
      Toast.show(msg)
      setIsLoader(false);
    }
  }

  return (
    <Modal
      visible={modalOpen}
      transparent={true}
      onRequestClose={() => {
        setModalOpen(false);
        navigation.navigate('MyDrivers');
      }}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Add Fuel</Text>
          </View>

          <TextLabel title={'Add Fuel'} />
          <TextInput
            style={styles.textInput}
            placeholder={'Add Quantity'}
            keyboardType={'phone-pad'}
            maxLength={4}
            value={quantity}
            placeholderTextColor={'#8D92A3'}
            onChangeText={text => { setQuantity(text) }}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 20 }}>
            <TouchableOpacity
              style={styles.touch}
              onPress={() => navigation.navigate('MyDrivers')}>
              <Text style={styles.touchtext}>CANCEL</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.touched}
              onPress={() => { upDateDriverQuantiApi() }}>
              {isLoader ?
                <ActivityIndicator size={'small'} color='#fff' />
                :
                <Text style={[styles.touchtext, { color: '#ffffff' }]}>SAVE</Text>
              }
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default Topup;
const TextLabel = ({ title }) => <Text style={styles.textLabel}>{title}</Text>;

const styles = StyleSheet.create({
  modal_View: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#00000060',
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    // marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  mdtop_1: {
    backgroundColor: '#31B9EB',
    // padding: 10,
    paddingVertical: 15,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 20,
  },
  mdTopText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  textLabel: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#7A7A7A',
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 0,
  },
  textInput: {
    borderRadius: 8,
    backgroundColor: '#727C8E28',
    fontSize: 15,
    fontFamily: 'Aviner-Medium',
    fontFamily: '500',
    color: '#000',
    marginHorizontal: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  touch: {
    borderRadius: 5,
    borderWidth: 1,
    height: 45,
    borderColor: '#E02020',
    width: "45%",
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy', fontSize: 15, fontWeight: 'bold', textAlign: 'center', color: '#E02020',
  },
  touched: {
    borderRadius: 5, height: 45, backgroundColor: '#ED6E1E', width: "45%",
    alignItems: 'center', justifyContent: 'center', marginBottom: 10
  }
});
