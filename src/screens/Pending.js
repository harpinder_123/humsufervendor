import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Dimensions, Image, FlatList, Modal, StyleSheet, Text, TouchableOpacity, View, Platform } from 'react-native';
import Dash from 'react-native-dash';
import CustomModal from '../Custom/CustomModal';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { ButtonStyle } from '../Custom/CustomView';
import { Api } from '../services/Api';
import Toast from 'react-native-simple-toast';
import moment from 'moment';

const { height } = Dimensions.get('window');

const Pending = ({ navigation }) => {
  const [isLoding, setIsLoding] = useState(true);
  const [isLodingModal, setIsLodingModal] = useState(false);
  const [isLodingModal2, setIsLodingModal2] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [assingValue, setAssingValue] = useState('');
  const [assignIndex, setAssignIndex] = useState(null);
  const [selectAssign, setSelectAssign] = useState('');
  const [assignCardQuantity, setAssignCardQuantity] = useState('');
  const [assignDriverQuantity, setAssignDriverQuantity] = useState('');
  const [assignDriverId, setAssignDriverId] = useState();
  const [assignModalVisible, setAssignModalVisible] = useState(false)
  const [pendingData, setPendingData] = useState([]);
  const [assignDriverData, setAssignDriverData] = useState([]);

  useEffect(() => {
    pendingHomeApi();
    getProfileApi()
    const unsubscribe = navigation.addListener('focus', () => {
      pendingHomeApi();
      getProfileApi()
    });
    return unsubscribe;
  }, [navigation])

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }

  const pendingHomeApi = async () => {
    const body = {
      condition: 'upcoming'
    };
    const response = await Api.home(body);
    const { status = false, orders = [] } = response;
    setTimeout(() => {
      setIsLoding(false)
      setPendingData(orders);
    }, 2000);
  };

  const assignDriverListFounction = async (orderId, index) => {
    setIsLodingModal(true)
    setSelectAssign(orderId)
    assignModalFunction(true)
    const response = await Api.driverList();
    const { status = false, drivers = [] } = response;
    if (status) {
      setAssingValue(index)
      setIsLodingModal(false)
      setAssignDriverData(drivers);
    }
  };

  const assignModalFunction = (visible) => {
    setAssignModalVisible(visible)
  }

  const submitFunction = async () => {
    if (assignIndex === null) {
      Toast.show('Please select Driver');
      return;
    } else {
      if (assignCardQuantity <= assignDriverQuantity) {
        const body = {
          order_id: selectAssign,
          driver_id: assignDriverId
        };
        setIsLodingModal2(true);
        const response = await Api.assignDriverList(body);
        const { status = false, msg } = response;
        if (status) {
          setIsLodingModal2(false);
          setAssignModalVisible(false);
          setModalOpen(false);
          Toast.show(msg)
          // orderAssignArrayValue(assingValue)
        } else {
          Toast.show(msg)
          setIsLodingModal2(false);
          setAssignIndex(null);
          setModalOpen(false);
          setAssignModalVisible(false);
          setAssignCardQuantity('');
          setAssignDriverQuantity('');
        }
      } else {
        Toast.show('Sorry ! Driver does not have sufficent fuel. Please add quantity first.')
        setIsLodingModal2(false);
        setAssignIndex(null);
        setModalOpen(false);
        setAssignModalVisible(false);
        setAssignCardQuantity('');
        setAssignDriverQuantity('');
      }
    }
  }

  const startOrderApi = async () => {
    const body = {
      orderid: selectAssign,
    };
    console.log('-----boby: ', body);
    const response = await Api.startOrder(body);
    const { status = false, msg } = response;
    if (status) {
      setModalOpen(false);
      Toast.show(msg)
      navigation.navigate('Ongoing');
      console.log('-----response: ', response);
    } else {
      Toast.show(response.data.msg)
      console.log('-----errree: ', response);
    }
  }

  const confirmationModal = () => {
    return (
      <Modal
        visible={modalOpen}
        animationType={'fade'}
        transparent={true}
        onRequestClose={() => {
          setModalOpen(false);
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <Text style={styles.text}>Confirmation</Text>
            <Text style={styles.subtext}>
              Do you want to change the status of order ?
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, paddingHorizontal: 20 }}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.touch}
                onPress={() => { setModalOpen(false) }}>
                <Text style={styles.touchtext}>CANCEL</Text>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.touched}
                onPress={() => { startOrderApi() }}>
                <Text style={[styles.touchtext, { color: '#ffffff', fontSize: 16 }]}>Yes</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  const selectDriverFunction = (driverID, driverIndex) => {
    setAssignDriverId(driverID)
    setAssignIndex(driverIndex)
  }
  const assignModalfunction = () => {
    return (
      <Modal
        visible={assignModalVisible}
        transparent={true}
        onRequestClose={() => {
          setAssignModalVisible(false)
        }}>
        <View style={styles.modalViewOffCss}>
          <View style={styles.modalCardViewOffCss}>
            <View style={styles.assignDriViewOffCss}>
              <Text style={styles.assignDriverTextOffCss}>Assign Driver:</Text>
            </View>
            <View style={{ marginHorizontal: 15, paddingVertical: 10, }}>
              {isLodingModal ?
                <View style={{ flex: 1, height: 60, justifyContent: 'center', backgroundColor: 'red' }}>
                  <ActivityIndicator size={'small'} color='#000' />
                </View>
                :
                <FlatList
                  data={assignDriverData}
                  renderItem={({ item, index }) => {
                    return (
                      <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.driverViewOffCss}
                        onPress={() => { selectDriverFunction(item.id, index), setAssignDriverQuantity(item.quantity) }}>
                        <View>
                          <Text style={[styles.assignDriverTextOffCss, { color: '#9F9F9F' }]}>{item.name}</Text>
                          <Text style={[styles.assignDriverTextOffCss, { color: '#9F9F9F', fontSize: 14 }]}>{item.user_info.vehicle_number}</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                          <Image style={{ width: 20, height: 20, resizeMode: 'contain' }}
                            source={
                              assignIndex === index ?
                                require('../images/radio.png')
                                :
                                require('../images/unradio.png')
                            } />
                        </View>
                      </TouchableOpacity>
                    )
                  }}
                />
              }
              <View style={{ paddingVertical: 10, marginHorizontal: 10, marginTop: 10 }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => { submitFunction() }}
                  style={styles.submitButtonViewOff}>
                  {isLodingModal2 ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                      <ActivityIndicator size={'small'} color='#FFF' />
                    </View>
                    :
                    <Text style={styles.submitTextOffCss}>SUBMIT</Text>
                  }
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal >
    )
  }

  const pendingFunctionList = ({ item, index }) => {
    moment.locale('en');
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.cardViewOffCss}
        onPress={() => { navigation.navigate("OrderDetail", { cardDetails: item }) }}>
        <View style={styles.ftview}>
          <Text style={styles.ftText}>Order: #{item.id}</Text>
          {/* <Text style={styles.ftText1}>{item.delivery_date}</Text> */}
          <Text style={styles.ftText1}>{moment(item.delivery_date, "YYYY-MM-DD, h:mm a").format("DD/MM/YYYY - h:mm A")}</Text>

        </View>
        <DashLine />
        {confirmationModal()}
        <Text style={[styles.ftText2, { marginHorizontal: 10, paddingVertical: 0 }]}>Status:
          <Text style={[styles.ftText2, { color: '#31B9EB', marginRight: 10, marginLeft: 10, paddingVertical: 2 }]}> Pending</Text>
        </Text>

        <View style={styles.ftview_1}>
          <View>
            <Text style={styles.ftText2}>Name</Text>
            <Text style={styles.ftText2}>Mobile No.</Text>
            <Text style={styles.ftText2}>Delivery Address</Text>
            <Text style={styles.ftText2}>Quantity (in Litres)</Text>
          </View>
          <View>
            <Text style={styles.ftText_3}>{item.order_by_detail?.name}</Text>
            <Text style={styles.ftText_3}>{item.order_by_detail?.phone}</Text>
            <Text numberOfLines={1} style={[styles.ftText_3, { width: 180 }]}>{item.delivery_address_detail?.address}</Text>
            <Text style={styles.ftText_3}>{item.quantity} LTR</Text>
          </View>
        </View>

        <DashLine />
        <View style={[styles.ftview_1, { marginTop: 0 }]}>
          <Text style={styles.ftText}>
            Total:
            <Text style={[styles.total, { color: '#F87B00' }]}> ₹{item.total_amount}/-</Text>
          </Text>

          {item.status == 'dispatched' ?
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => { setSelectAssign(item.id), setModalOpen(true); }}
              style={{ width: 70, height: 26, backgroundColor: '#0c9e5f', justifyContent: 'center', alignItems: 'center', borderRadius: 4 }}>
              <Text style={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'Avenir-Heavy', fontWeight: '900' }}>START</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => { assignDriverListFounction(item.id, index), setAssignCardQuantity(item.quantity) }}
              style={{ width: 70, height: 26, backgroundColor: '#ED6E1E', justifyContent: 'center', alignItems: 'center', borderRadius: 4 }}>
              <Text style={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'Avenir-Heavy', fontWeight: '900' }}>ASSIGN</Text>
            </TouchableOpacity>
          }
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={{ flex: 1 }}>
      <StatusBarLight />
      {assignModalfunction()}
      {isLoding ?
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size={'large'} color='#000' />
        </View>
        :
        pendingData.length > 0 ?
          <FlatList
            data={pendingData}
            renderItem={pendingFunctionList}
            showsVerticalScrollIndicator={false}
          />
          :
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={styles.notFoundTextOffCss}>Not Available</Text>
          </View>
      }

    </View>
  );
};

export default Pending;

const styles = StyleSheet.create({
  cardViewOffCss: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
    borderRadius: 10,
    marginHorizontal: 20,
    elevation: 4,
    marginVertical: 10
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#9F9F9F',
  },
  // dash: {
  //   borderColor: '#6F6F7B',
  //   marginHorizontal: 20,
  //   borderStyle: 'dotted',
  //   borderWidth: 1,
  //   marginTop: 10,
  // },
  ftText2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#9F9F9F',
    paddingVertical: 2
  },
  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    marginTop: 10,
    alignItems: 'center',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'flex-end',
    paddingVertical: 2
  },
  notFoundTextOffCss: {
    fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 18, color: '#000'
  },
  modalViewOffCss: {
    backgroundColor: '#00000080', flex: 1, justifyContent: 'center'
  },
  modalCardViewOffCss: {
    backgroundColor: '#fff', elevation: 5, marginHorizontal: 20, borderRadius: 5
  },
  assignDriViewOffCss: {
    paddingVertical: 10, paddingHorizontal: 10, backgroundColor: '#31B9EB',
    borderTopLeftRadius: 5, borderTopRightRadius: 5,
  },
  assignDriverTextOffCss: {
    fontFamily: 'Avenir-Heavy', fontSize: 20, fontWeight: '900', color: '#FFFFFF',
  },
  driverViewOffCss: {
    marginHorizontal: 10, flexDirection: 'row', justifyContent: 'space-between',
    borderBottomColor: '#C8C8D3', borderBottomWidth: 0.6, paddingVertical: 10
  },
  submitButtonViewOff: {
    backgroundColor: '#ED6E1E', borderRadius: 20, paddingVertical: 10,
    justifyContent: 'center', alignItems: 'center', height: 42,
  },
  submitTextOffCss: {
    color: '#FFFFFF', fontSize: 16, fontWeight: '900', fontFamily: 'Avenir-Heavy'
  },
  modal_View: {
    flex: 1,
    backgroundColor: Platform.OS === 'ios' ? '#00000060' : '#00000015',
    justifyContent: 'center'
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    // marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginTop: 10,
    marginHorizontal: 20,
  },
  subtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#6F6F7B',
    marginTop: 5,
    marginBottom: 20,
    marginHorizontal: 20,
  },
  touch: {
    borderRadius: 5,
    height: 45,
    width: "45%",
    backgroundColor: '#FF0000',
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
  },
  touched: {
    borderRadius: 5,
    height: 45,
    width: "45%",
    backgroundColor: '#0c9e5f',
    alignItems: 'center',
    justifyContent: 'center'
  },
});


export const DashLine = props => (
  <View style={{ margin: props.margin ? props.margin : 10 }}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);