import React, { useState, useEffect } from 'react';
import { FlatList, Modal, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import CustomModal from '../Custom/CustomModal';
import Dash from 'react-native-dash';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { ButtonStyle } from '../Custom/CustomView';
import { Api } from '../services/Api';
import Toast from 'react-native-simple-toast';
import moment from 'moment';

const Ongoing = ({ navigation }) => {
  const [isLoding, setIsLoding] = useState(true);
  const [modalOpen, setModalOpen] = useState(false);
  const [ongoingData, setOngoingData] = useState([]);
  const [selectAssign, setSelectAssign] = useState();
  useEffect(() => {
    ongoingHomeApi();
    getProfileApi()
    const unsubscribe = navigation.addListener('focus', () => {
      ongoingHomeApi();
      getProfileApi()
    });
    return unsubscribe;
  }, [navigation])

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }

  const ongoingHomeApi = async () => {
    const body = {
      condition: 'ongoing'
    };
    const response = await Api.home(body);
    const { status = false, orders = [] } = response;
    setTimeout(() => {
      setIsLoding(false)
      setOngoingData(orders);
    }, 2000);
  };

  const startOrderApi = async () => {
    const body = {
      orderid: selectAssign,
    };
    const response = await Api.completedOrder(body);
    const { status = false, msg, data } = response;
    if (status) {
      setModalOpen(false);
      Toast.show(msg)
      navigation.navigate('Completed')
      console.log('-----res: ', response);
    } else {
      Toast.show(data.msg)
      setModalOpen(false);
      console.log('-----res else: ', response);
    }
  }

  const confirmationModal = () => {
    return (
      <Modal
        visible={modalOpen}
        animationType={'fade'}
        transparent={true}
        onRequestClose={() => { setModalOpen(false) }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <Text style={styles.text}>Confirmation</Text>
            <Text style={styles.subtext}>
              Do you want to change the status of order ?
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, paddingHorizontal: 20 }}>
              <TouchableOpacity
                style={styles.touch}
                onPress={() => { setModalOpen(false) }}>
                <Text style={styles.touchtext}>CANCEL</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.touched}
                onPress={() => { startOrderApi() }}>
                <Text style={[styles.touchtext, { color: '#ffffff', fontSize: 16 }]}>Yes</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  const ongingFunctionList = ({ item, index }) => {
    moment.locale('en');
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.cardViewOffCss}
        onPress={() => { navigation.navigate("OrderDetail", { cardDetails: item }) }}>
        <View style={styles.ftview}>
          <Text style={styles.ftText}>Order: #{item.id}</Text>
          <Text style={styles.ftText1}>{moment(item.delivery_date, "YYYY-MM-DD, h:mm a").format("DD/MM/YYYY - h:mm A")}</Text>
        </View>
        <DashLine />
        <Text style={[styles.ftText2, { marginHorizontal: 10, paddingVertical: 0 }]}>Status:
          <Text style={[styles.ftText2, { color: '#ED6E1E', marginRight: 10, marginLeft: 10, paddingVertical: 2 }]}> Ongoing</Text>
        </Text>
        <View style={styles.ftview_1}>
          <View>
            <Text style={styles.ftText2}>Name</Text>
            <Text style={styles.ftText2}>Mobile No.</Text>
            <Text style={styles.ftText2}>Delivery Address</Text>
            <Text style={styles.ftText2}>Driver Name</Text>
            <Text style={styles.ftText2}>Truck Number</Text>
            <Text style={styles.ftText2}>Quantity (in Litres)</Text>
          </View>
          <View>
            <Text style={styles.ftText_3}>{item.order_by_detail?.name}</Text>
            <Text style={styles.ftText_3}>{item.order_by_detail?.phone}</Text>
            <Text numberOfLines={1} style={[styles.ftText_3, { width: 180 }]}>{item.delivery_address_detail.address}</Text>
            <Text style={styles.ftText_3}>{item.driver_details?.name}</Text>
            <Text style={styles.ftText_3}>{item.driver_details?.driver_info?.vehicle_number}</Text>
            <Text style={styles.ftText_3}>{item.quantity} LTR</Text>
          </View>
        </View>
        <DashLine />
        <View style={[styles.ftview_1, { marginTop: 0 }]}>
          <Text style={styles.ftText}>
            Total:
            <Text style={[styles.total, { color: '#F87B00' }]}> ₹{item.total_amount}/-</Text>
          </Text>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => { setModalOpen(true), setSelectAssign(item.id) }}
            style={{ width: 75, height: 25, backgroundColor: '#ED6E1E', justifyContent: 'center', alignItems: 'center', borderRadius: 4 }}>
            <Text style={{ color: '#FFFFFF', fontSize: 12, fontFamily: 'Avenir-Heavy', fontWeight: '900' }}>DISPATCH</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={{ flex: 1 }}>
      <StatusBarLight />
      {isLoding ?
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size={'large'} color='#000' />
        </View>
        :
        ongoingData.length > 0 ?
          <FlatList
            data={ongoingData}
            renderItem={ongingFunctionList}
            showsVerticalScrollIndicator={false}
          />
          :
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={styles.notFoundTextOffCss}>Not Available</Text>
          </View>
      }
      {confirmationModal()}
    </View>
  );
};

export default Ongoing;

const styles = StyleSheet.create({
  cardViewOffCss: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
    borderRadius: 10,
    marginHorizontal: 20,
    elevation: 4,
    marginVertical: 10
  },
  ftview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  ftText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#1E1F20',
  },
  ftText1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '600',
    color: '#9F9F9F',
  },
  // dash: {
  //   borderColor: '#6F6F7B',
  //   marginHorizontal: 20,
  //   borderStyle: 'dotted',
  //   borderWidth: 1,
  //   marginTop: 10,
  // },
  ftText2: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#9F9F9F',
    paddingVertical: 2
  },
  ftview_1: {
    flexDirection: 'row',
    marginHorizontal: 10,
    justifyContent: 'space-between',
    marginTop: 10,
    alignItems: 'center',
  },
  ftText_3: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'flex-end',
    paddingVertical: 2
  },
  notFoundTextOffCss: {
    fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 18, color: '#000'
  },
  modal_View: {
    flex: 1,
    backgroundColor: '#00000060',
    justifyContent: 'center'
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    // marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginTop: 10,
    marginHorizontal: 20,
  },
  subtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#6F6F7B',
    marginTop: 5,
    marginBottom: 20,
    marginHorizontal: 20,
  },
  touch: {
    borderRadius: 5,
    height: 45,
    width: "45%",
    backgroundColor: '#FF0000',
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
  },
  touched: {
    borderRadius: 5,
    height: 45,
    width: "45%",
    backgroundColor: '#0c9e5f',
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export const DashLine = props => (
  <View style={{ margin: 10 }}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);

const ModalView = props => (
  <Modal visible={modalOpen} transparent={true} {...props}></Modal>
);