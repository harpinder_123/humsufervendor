import React, { useEffect, useState } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View, Linking
} from 'react-native';
import { StatusBarLight } from '../Custom/CustomStatusbar';
import { Header } from '../Custom/CustomView';
import { LocalStorage } from '../services/Api';

const Support = ({ navigation }) => {
  const [checked, setChecked] = useState('first');
  const [userData, setUserData] = useState("")

  useEffect(() => {
    LocalStorage.getUserDetail('userdata').then((userdata) => {
      setUserData(JSON.parse(userdata))
    })
  }, [])

  return (
    <View style={{ flex: 1 }}>
      <StatusBarLight />
      <Header onPress={() => navigation.goBack()} title="Support" />
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}>
        <Image
          style={styles.image}
          source={require('../images/vendor-app-slice/support.png')}
        />
        <Text style={styles.text}>Get Support</Text>
        <Text style={styles.subtext}>
          For any support request regards your{`\n`} orders please feel free to
          speak{`\n`} with us at below.
        </Text>
        <Text style={styles.sub2text}>
          {/* {JSON.parse(userdata)} */}
          Call us - +91 {userData.user_detail?.phone} {`\n`}Mail us - {userData.user_detail?.email}
        </Text>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.touch}
          onPress={() => {
            Linking.openURL('tel:' + userData.user_detail?.phone);
          }}>
          <Text style={styles.touchtext}>CALL US</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Support;

const styles = StyleSheet.create({
  image: {
    width: 160,
    height: 175,
    alignSelf: 'center',
    marginTop: 60,
    resizeMode: 'contain'
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 40,
    color: '#1E1F20'
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    lineHeight: 28,
    marginTop: 20,
    color: '#747A8D',
    textAlign: 'center',
  },
  sub2text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    lineHeight: 30,
    color: '#1E1F20',
    textAlign: 'center',
    marginTop: 80,
  },
  touch: {
    // padding: 10,
    paddingVertical: 8,
    marginHorizontal: 100,
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    marginTop: 40,
    marginBottom: 20
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ffffff',
    textAlign: 'center',
  },
});
