import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';
import { Header } from '../Custom/CustomView';
import { Api } from '../services/Api';
import moment from 'moment';
import { ActivityIndicator } from 'react-native-paper';
const { height } = Dimensions.get('window');

const Notification = ({ navigation }) => {
  const [isLoader, setIsLoader] = useState(true);
  const [notifyData, setNotifyData] = useState([])

  useEffect(() => {
    driverSealApi()
    getProfileApi()
  }, [])

  const getProfileApi = async () => {
    const response = await Api.getProfile();
    const { status = false, vendor_info = [] } = response;
    if (vendor_info[0].status === 0) {
      navigation.replace('Login');
    }
  }

  const driverSealApi = async () => {
    let body = {}
    const response = await Api.notificationApi(body);
    const { status = false, msg, list = [] } = response;
    if (status) {
      setIsLoader(false);
      setNotifyData(list)
      // alert(msg);
      console.log('------: ', list.length);

    } else {
      // alert(msg);
      setIsLoader(false);
    }
  }

  const notifyList = (item, index) => {
    moment.locale('en');
    var dateTime = item.created_at
    return (
      <View style={styles.nlBg}>
        <View style={styles.topList}>
          <View style={styles.line} />
          <View style={{ paddingVertical: 10, paddingHorizontal: 12, alignItems: 'center' }}>
            <Image
              source={require('../images/bell.png')}
              style={styles.listImage}
            />
          </View>
          <View style={{ width: "78%", paddingVertical: 10 }}>
            <Text style={styles.listText}>{item.title}</Text>
            <Text style={styles.listText_1}>{moment(dateTime).format('DD MMMM YYYY')}</Text>
          </View>
        </View>
      </View>
    )
  }

  return (
    <View styles={styles.containr}>
      <Header onPress={() => navigation.goBack()} title={'Notification'} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ height: height / 1.10, paddingHorizontal: 20, marginBottom: 50, backgroundColor: '#F7F7F7' }}>
        {isLoader ?
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator size={'small'} color={'#000'} />
          </View>
          :
          notifyData.length > 0 ?
            notifyData.map((item, index) => {
              return (
                notifyList(item, index)
              )
            })
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.notFoundTextOffCss}>Not Available</Text>
            </View>
        }
      </ScrollView>
    </View>
  );
};

export default Notification;

const styles = StyleSheet.create({
  containr: {
    backgroundColor: '#F7F7F7',
    flex: 1,
  },
  nlBg: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 5,
    marginTop: 15
  },
  line: {
    width: '2%',
    backgroundColor: '#ED6E1E',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  topList: {
    flexDirection: 'row',
  },
  listImage: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    // margin: 10,
  },
  listText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000521',
    // paddingTop: 10    
  },
  listText_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#ED6E1E',
    marginTop: 10,
    // marginBottom: 10,
  },
  notFoundTextOffCss: {
    fontFamily: 'Avenir-Heavy', fontWeight: '900', fontSize: 18, color: '#000'
  }
});
