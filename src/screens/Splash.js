import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Image, Dimensions, Modal, PermissionsAndroid, Platform } from 'react-native';
import { StatusBarDark } from '../Custom/CustomStatusbar';
import { Api, LocalStorage } from '../services/Api';
import { _RemoveAuthToken, _SetAuthToken } from '../services/ApiSauce';
import { useDispatch } from 'react-redux';
import * as actions from '../redux/actions';
import messaging from '@react-native-firebase/messaging';
import Geolocation from 'react-native-geolocation-service';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';

const Splash = ({ navigation }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    navigationHandler();
    requestUserPermission();
    createNotificationListeners();
    getCurrentPosition();
  }, []);

  const navigationHandler = async () => {
    // navigation.replace('Login');
    const token = (await LocalStorage.getToken()) || '';
    if (token.length !== 0) {
      // console.log({ token });
      _SetAuthToken(token);
      const response = await Api.getProfile();
      const { status = false, user_profile = {}, vendor_info = [] } = response;
      // console.log('-------getProfile: ', response)
      if (status) {
        dispatch(actions.SetUserDetail(user_profile[0]));
        console.log('-----status if: ', vendor_info[0].register_verification)
        if (vendor_info[0].register_verification == 1 && vendor_info[0].status == 1) {
          // console.log('-----status if: ',)
          const response2 = await Api.fuelUpdatePriceApi();
          const { status2 = false, change_flag } = response2;
          console.log('-----change_flag: ', change_flag)
          if (change_flag === 1) {
            navigation.replace('UpdateFuelPrice', { type: 'login' })
            console.log('-------UpdateFuelPrice: ')
          } else {
            setTimeout(() => {
              navigation.replace('DrawerNavigator');
              console.log('-------DrawerNavigator: ')
            }, 3000);
          }
        }
        // } else if (vendor_info[0].register_verification === 0) {
        //   setModalOpen(true);
        //   console.log('-------resgition alert: ')
        // } else if (vendor_info[0].status === 0) {
        //   // alert('is hi scree pe rekhana hai')
        //   console.log('-------is hi screen pe rekhana hai ')
        // }
      } else {
        _RemoveAuthToken();
        LocalStorage.setToken('');
        navigation.replace('Login');
        console.log('-------status ka else ')
      }
    } else {
      setTimeout(() => {
        navigation.replace('Login');
        console.log('-------token ka else ')
      }, 3000);
    }
  };

  const getCurrentPosition = async () => {
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }
    if (Platform.OS === 'ios') {
      console.log('----------ios platform',);
      iosGetLocation();
    }
    Geolocation.getCurrentPosition((position) => {
      dispatch(actions.LatlongInfo(position.coords.latitude));
      dispatch(actions.LongInfo(position.coords.longitude));
      console.log('-----positon2: ', position.coords.latitude, ' ------------ ', position.coords.longitude)
    },
      (error) => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  const iosGetLocation = () => {
    Geolocation.getCurrentPosition((position) => {
      dispatch(actions.LatlongInfo(position.coords.latitude));
      dispatch(actions.LongInfo(position.coords.longitude));
      console.log('-----positon2: ', position.coords.latitude, ' ------------ ', position.coords.longitude)
    },
      (error) => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      getFcmToken();
    }
  }

  const getFcmToken = async () => {
    try {
      let fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('+++++++fcmToken:  ' + JSON.stringify(fcmToken));
        LocalStorage.setFcmToken(fcmToken)
      }
    } catch (error) {
      console.log(error, '-------------error');
    }
  };

  const createNotificationListeners = async () => {
    // notification background
    await messaging().setBackgroundMessageHandler(async notification => {
      console.log('--------------Notification 11:', notification)
      let notiData = notification.data;
    });
    // When the application is running, but in the background
    await messaging().onMessage(async notification => {
      console.log('----------teste:', notification);
      let notiData = notification.notification;
      // PushNotification.localNotification({
      //   channelId: 'default_channel_1',
      //   title: notiData.title, // (optional)
      //   message: notiData.body, // (required)
      //   vibrate: true,
      //   vibration: 300,
      //   playSound: true,
      //   soundName: 'my_sound.mp3',
      // })
      // await messaging().onMessage(async notification => {
      //   console.log('--------------Notification 23:', notification)
      // let notiData = notification.notification;
      // PushNotification.localNotification({
      //   channelId: 'default_channel_1',
      //   title: notiData.title, // (optional)
      //   message: notiData.body, // (required)
      //   vibrate: true,
      //   vibration: 300,
      //   playSound: true,
      //   soundName: 'my_sound.mp3',
      // })
      {
        Platform.OS == "android" ?
          PushNotification.localNotification({
            channelId: 'default_channel_1',
            title: notiData.title, // (optional)
            message: notiData.body, // (required)
            vibrate: true,
            vibration: 300,
            playSound: true,
            soundName: 'my_sound.mp3',
          })
          :
          PushNotificationIOS.presentLocalNotification({
            alertTitle: notiData.title,
            alertBody: notiData.body,
            vibrate: true,
            vibration: 300,
            playSound: true,
            soundName: 'my_sound.mp3',
          });
      }
      // })
    });

    // notification app kill
    await messaging().getInitialNotification().then(async notification => {
      console.log('--------------Notification 33:', notification);
      let notiData = notification;
    });
  }

  return (
    <View style={{ backgroundColor: '#FFFFFF', flex: 1, justifyContent: 'center' }}>
      <StatusBarDark />
      <Image style={styles.image} source={require('../images/logo.png')} />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  image: {
    // marginTop: height / 3,
    width: 219,
    height: 232,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});
