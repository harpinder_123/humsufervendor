import React, { useState, useEffect } from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { BottomButton, BottomView, MainImage } from '../Custom/CustomView';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomInputField } from './Login';
import Loader from '../Custom/Loader';
import { Api, LocalStorage } from '../services/Api';
import { _SetAuthToken } from '../services/ApiSauce';
import * as actions from '../redux/actions';
import Toast from 'react-native-simple-toast';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';
import { useDispatch, useSelector } from 'react-redux';

const Register2 = ({ navigation, route }) => {
  const [personalDetails, setPersonalDetails] = useState(route.params?.item)
  const [petrolValue, setPetrolValue] = useState([{ value: 'No' }, { value: 'Yes' }]);
  const [dieselValue, setDieselValue] = useState([{ value: 'No' }, { value: 'Yes' }]);

  const [state, setState] = useState({
    Latitude: '',
    Longitude: '',
    WantToSellPetrol: null,
    WantToSellDiesel: null,
    password: '',
    isLoading: false,
    // ...route.params,
    mobileforward: '',
    emailforward: '',
    nameforward: '',
    devicetoken: '',
    setValidation: false
  });

  const { latlongInfo, longInfo } = useSelector(store => store)

  useEffect(() => {
    LocalStorage.getFcmToken('fcmToken').then((fcmToken) => {
      setState({ ...state, devicetoken: fcmToken })
    })
  }, [navigation])

  const onSubmitHandler = async () => {
    const {
      Latitude,
      Longitude,
      WantToSellPetrol,
      WantToSellDiesel,
      password,
      devicetoken
    } = state;

    if (WantToSellPetrol === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (WantToSellDiesel === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (password === '') {
      setState({ ...state, setValidation: true });
      return;
    }
    var val = Math.floor(1000 + Math.random() * 9000);
    LocalStorage.setRandomNum(JSON.stringify(val))
    console.log('-------val : ', val);
    const body = {
      name: personalDetails.name,
      contact_person: personalDetails.contact_person,
      proprietor_name: personalDetails.proprietor_name,
      email: personalDetails.email,
      omc: personalDetails.omc,
      city: personalDetails.city,
      state: personalDetails.state,
      phone: personalDetails.phone,
      paytm_number: personalDetails.paytm_number,
      tin_no: personalDetails.tin_no,
      gst_no: personalDetails.gst_no,
      pincode: personalDetails.pincode,
      bank_name: personalDetails.bank_name, // feel krna hai
      account_holder_name: personalDetails.account_holder_name,
      branch_name: personalDetails.branch_name,
      account_number: personalDetails.account_number,
      ifsc_code: personalDetails.ifsc_code,
      latitude: Latitude ? Latitude : latlongInfo,
      longitude: Longitude ? Longitude : longInfo,
      selling_petrol: WantToSellPetrol,
      selling_diesel: WantToSellDiesel,
      password: password,
      device_type: Platform.OS === 'ios' ? 'IOS' : "Android",
      devicetoken: devicetoken,
    };
    console.log('-------body : ', body);
    setState({ ...state, isLoading: true });
    const response = await Api.otp({
      otp: String(val),
      phone: personalDetails.phone,
      email: personalDetails.email,
      name: personalDetails.name,
    });
    console.log('-------otp : ', response);

    const {
      status = false,
      msg
    } = response;
    consolejson(response);
    if (status) {
      Toast.show(msg);
      navigation.replace('Otp', { item: body });
      setState({ ...state, isLoading: false });
    } else {
      const {
        data: { msg = 'Something went wrong' },
      } = response;
      Toast.show(msg);
      setState({ ...state, isLoading: false });
    }
  };


  const onChangeDropdownPetroleType = (value, index, data) => {
    setState({ ...state, WantToSellPetrol: index })
    console.log('----Petrol: ', index);
  };

  const onChangeDropdownDiesalType = (value, index, data) => {
    setState({ ...state, WantToSellDiesel: index })
    console.log('----Diesal: ', index);
  };


  return (
    <KeyboardAwareScrollView showsVerticalScrollIndicator={true}>
      <MainImage height={"100%"} bottom={"0%"}>
        <BottomView>
          <Text style={styles.topText}>Register</Text>
          <Text style={styles.topText_1}>Enter your basic details</Text>

          <CustomInputField
            tfSource={require('../images/compass.png')}
            label="Latitude"
            width={16}
            height={16}
            value={state.Latitude ? state.Latitude : latlongInfo}
            keyboardType={'number-pad'}
            onChangeText={Latitude => { setState({ ...state, Latitude: Latitude }) }}
          />
          {/* {state.setValidation == true && latlongInfo === '' ?
            <Text style={styles.validationStyle}>Please enter Lattitude</Text>
            : <></>
          } */}

          <CustomInputField
            tfSource={require('../images/compass.png')}
            label="Longitude"
            width={16}
            height={16}
            value={state.Longitude ? state.Longitude : longInfo}
            keyboardType={'number-pad'}
            onChangeText={Longitude => { setState({ ...state, Longitude: Longitude }) }}
          />
          {/* {state.setValidation == true && longInfo === '' ?
            <Text style={styles.validationStyle}>Please enter Longitude</Text>
            : <></>
          } */}

          {/* <CustomInputField
            tfSource={require('../images/petrol-station.png')}
            label="Want to sell Petrol ?"
            width={16}
            height={16}
            value={state.WantToSellPetrol}
            onChangeText={WantToSellPetrol =>
              setState({ ...state, WantToSellPetrol })
            }
          /> */}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ width: "10%", justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 16, height: 16 }} source={require('../images/petrol-station.png')} />
            </View>
            <View style={{ width: "85%", }}>
              <Dropdown
                style={styles.drops}
                itemColor={'rgba(0, 0, 0, .54)'}
                underlineColor="transparent"
                label={'Want to sell Petrol ?'}
                // icon="cheveron-down"
                iconColor="rgba(0, 0, 0, 1)"
                icon={require('../images/next.png')}
                dropdownOffset={{ top: 32, left: 0 }}
                dropdownMargins={{ min: 8, max: 16 }}
                pickerStyle={{ width: '88%', left: '6%', marginTop: 15 }}
                dropdownPosition={-2.40}
                shadeOpacity={0.12}
                rippleOpacity={0.4}
                baseColor={'white'}
                data={petrolValue}
                onChangeText={(value, index, data) => {
                  onChangeDropdownPetroleType(value, index, data);
                }}
              />
            </View>
          </View>
          {state.setValidation == true && state.WantToSellPetrol === null ?
            <Text style={styles.validationStyle}>Please enter your Sell Patrol</Text>
            : <></>
          }

          {/* <CustomInputField
            tfSource={require('../images/diesel.png')}
            label="Want to sell Diesel ?"
            width={16}
            height={16}
            value={state.WantToSellDiesel}
            onChangeText={WantToSellDiesel =>
              setState({ ...state, WantToSellDiesel })
            }
          /> */}
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ width: "10%", justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 16, height: 16 }} source={require('../images/diesel.png')} />
            </View>
            <View style={{ width: "85%", }}>
              <Dropdown
                style={styles.drops}
                itemColor={'rgba(0, 0, 0, .54)'}
                underlineColor="transparent"
                label={'Want to sell Diesel ?'}
                iconColor="rgba(0, 0, 0, 1)"
                icon={require('../images/next.png')}
                dropdownOffset={{ top: 32, left: 0 }}
                dropdownMargins={{ min: 8, max: 16 }}
                pickerStyle={{ width: '88%', left: '6%', marginTop: 15 }}
                dropdownPosition={-2.40}
                shadeOpacity={0.12}
                rippleOpacity={0.4}
                baseColor={'white'}
                data={dieselValue}
                onChangeText={(value, index, data) => {
                  onChangeDropdownDiesalType(value, index, data);
                }}
              />
            </View>
          </View>
          {state.setValidation == true && state.WantToSellDiesel === null ?
            <Text style={styles.validationStyle}>Please enter your Sell Diesel</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/lock.png')}
            label="Password"
            width={14}
            height={16}
            keyboardType={Platform.OS === 'ios' ? 'default': 'visible-password'}
            value={state.password}
            onChangeText={password => setState({ ...state, password })}
          />
          {state.setValidation == true && state.password === '' ?
            <Text style={styles.validationStyle}>Please enter your Password</Text>
            : <></>
          }

          <BottomButton loader={state.isLoading} bottomtitle="SUBMIT" onPress={() => { onSubmitHandler() }} />

          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.bottomText}>
              Already have an account?
              <Text style={styles.bottomText_1}> Login Now</Text>
            </Text>
          </TouchableOpacity>
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default Register2;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    marginLeft: 10,
    marginTop: 4,
  },
  otpInput: {
    // width: '84%',
    padding: 10,
    marginHorizontal: 10,
    marginTop: 20,
    height: 66,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#F9F9F9',
    backgroundColor: '#F9F9F9',
    borderRadius: 20,
    fontSize: 35,
    color: '#000000',
  },

  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#ED6E1E',
    fontSize: 35,
    color: '#FFFFFF',
    borderColor: '#E7E7E7',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
    alignSelf: 'center',
    marginTop: 10,
  },
  bottomText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
    marginTop: 4,
  },
  validationStyle: {
    fontFamily: 'Avenir-Medium', fontSize: 14, fontWeight: '500', color: '#FA7272', marginHorizontal: 35
  },
  drops: {
    height: 50,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#00000040',
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '600',
    color: '#000521',
    paddingHorizontal: 1,
  },
});
