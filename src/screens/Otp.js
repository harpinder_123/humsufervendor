import React, { useEffect, useState } from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View, Modal, Image } from 'react-native';
import { BottomButton, BottomView, MainImage } from '../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Loader from '../Custom/Loader';
import { Api, LocalStorage } from '../services/Api';
import { _SetAuthToken } from '../services/ApiSauce';
import { useDispatch } from 'react-redux';
import * as actions from '../redux/actions';
import Toast from 'react-native-simple-toast';

const Otp = ({ navigation, route }) => {
  const [getValue, setGetValue] = useState(route.params?.item)
  const [modalOpen, setModalOpen] = useState(false);
  const [state, setState] = useState({
    otp: '',
    randomOtp: '',
    isLoading: false,
  });

  useEffect(() => {
    // alert(JSON.stringify(route.params));
    LocalStorage.getRandomNum('random').then((random) => {
      setState({ ...state, randomOtp: JSON.parse(random) })

    })
  }, []);

  const dispatch = useDispatch();
  const toggleLoader = isLoading => setState({ ...state, isLoading });

  const onResendHandler = async () => {
    var val = Math.floor(1000 + Math.random() * 9000);
    LocalStorage.setRandomNum(JSON.stringify(val))
    toggleLoader(true);
    const response = await Api.otp({
      otp: String(val),
      phone: getValue.phone,
      email: getValue.email,
      name: getValue.name,
    });
    toggleLoader(false);
    const { status = false, msg } = response;
    if (status) {
      Toast.show(msg);
    } else {
      Toast.show('Something went wrong please try later');
    }
  };

  const onVerifyHandler = async () => {
    const { otp, randomOtp } = state;
    if (otp.length !== 4) {
      Toast.show('Please enter your valid otp');
      return;
    } else {
      if (randomOtp != otp) {
        Toast.show('Your OTP is not correct');
      }
      else {
        const body = {
          name: getValue.name,
          contact_person: getValue.contact_person,
          proprietor_name: getValue.proprietor_name,
          email: getValue.email,
          omc: getValue.omc,
          city: getValue.city,
          state: getValue.state,
          phone: getValue.phone,
          paytm_number: getValue.paytm_number,
          tin_no: getValue.tin_no,
          gst_no: getValue.gst_no,
          pincode: getValue.pincode,
          bank_name: getValue.bank_name, // feel krna hai
          account_holder_name: getValue.account_holder_name,
          branch_name: getValue.branch_name,
          account_number: getValue.account_number,
          ifsc_code: getValue.ifsc_code,
          latitude: getValue.latitude,
          longitude: getValue.longitude,
          selling_petrol: getValue.selling_petrol,
          selling_diesel: getValue.selling_diesel,
          password: getValue.password,
          device_type: Platform.OS === 'ios' ? 'IOS' : "Android",
          devicetoken: getValue.devicetoken,
        };
        console.log('-------body : ', body);
        setState({ ...state, isLoading: true });
        const response = await Api.signup(body);
        const { status = false, token = '', user_detail = {}, signup_skip = false } = response;
        console.log('-------res: ', response);
        if (status) {
          LocalStorage.setToken(token);
          _SetAuthToken(token);
          dispatch(actions.SetUserDetail(user_detail));
          setState({ ...state, isLoading: false });
          setModalOpen(true)
        } else {
          const {
            data: { msg = 'Something went wrong' },
          } = response;
          setState({ ...state, isLoading: false });
          Toast.show(msg);
        }
      }
    }
  };

  const registrationApproval = () => {
    return (
      <Modal
        visible={modalOpen}
        transparent={true}
        onRequestClose={() => {
          navigation.navigate('Login');
          setModalOpen(false);
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <Image style={{ width: 50, height: 50, resizeMode: 'contain', marginVertical: 7, marginTop: 13 }} source={require('../images/tick.png')} />
            <Text style={[styles.registerTextOffCss, { marginBottom: 5 }]}>Registration submitted for</Text>
            <Text style={styles.registerTextOffCss}>approval.</Text>
            <Text style={[styles.registerText2OffCss, { marginBottom: 5, }]}>After approval from admin you can</Text>
            <Text style={styles.registerText2OffCss}>use the app.</Text>
          </View>
        </View>
      </Modal>
    );
  };


  return (
    <KeyboardAwareScrollView>
      {/* <Loader status={state.isLoading} /> */}
      <MainImage height={"100%"} bottom={"0%"}>
        <BottomView>
          <Text style={styles.topText}>OTP</Text>
          <Text style={styles.topText_1}>
            We will send you confirmation code
          </Text>
          <OTPInputView
            style={styles.otpInput}
            pinCount={4}
            code={state.otp}
            onCodeChanged={code => setState({ ...state, otp: code })}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
          // onCodeFilled={() => navigation.navigate('Professional Details')}
          />
          {registrationApproval()}
          <BottomButton loader={state.isLoading} bottomtitle="VERIFY" onPress={() => { onVerifyHandler() }} />
          <Text style={styles.bottomText}>I did not recieve a code</Text>
          <TouchableOpacity onPress={() => { onResendHandler() }}>
            <Text style={styles.bottomText_1}>Resend</Text>
          </TouchableOpacity>
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default Otp;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    marginLeft: 10,
    marginTop: 4,
  },
  otpInput: {
    // width: '84%',
    padding: 10,
    marginLeft: 20,
    marginTop: 30,
    height: 66,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 70,
    height: 70,
    borderWidth: 1,
    borderColor: '#F9F9F9',
    backgroundColor: '#F9F9F9',
    borderRadius: 20,
    fontSize: 35,
    color: '#000000',
  },

  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#ED6E1E',
    fontSize: 35,
    color: '#FFFFFF',
    borderColor: '#E7E7E7',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#100C0850',
    alignSelf: 'center',
    marginTop: 10,
  },
  bottomText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
    marginTop: 4,
  },
  modal_View: {
    flex: 1,
    backgroundColor: '#777777',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mdtop: {
    width: "90%",
    backgroundColor: '#FFFFFF',
    borderRadius: 15,
    alignItems: 'center',
    bottom: "2%"
  },
  registerTextOffCss: {
    color: '#1E1F20', fontFamily: 'Avenir-Heavy', fontWeight: '700', fontSize: 18,
    textAlign: 'center', marginBottom: 20, paddingHorizontal: 40,
  },
  registerText2OffCss: {
    color: '#6F6F7B', fontFamily: 'Avenir-Heavy', fontWeight: '600', fontSize: 15,
    textAlign: 'center', marginBottom: 20, paddingHorizontal: 50,
  }
});
