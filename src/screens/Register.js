import React, { useState, useEffect } from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View, Image, Dimensions } from 'react-native';
import { BottomButton, BottomView, MainImage } from '../Custom/CustomView';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomInputField } from './Login';
import { Api, LocalStorage } from '../services/Api';
import { _SetAuthToken } from '../services/ApiSauce';
import { validateEmail } from '../services/utils';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';
import Toast from 'react-native-simple-toast';

const { width, height } = Dimensions.get('window')

const Register = ({ navigation }) => {
  const [selectJob, setSelectJob] = useState([]);
  const [state, setState] = useState({
    name: '',
    contact_person: '',
    proprietor_name: '',
    email_address: '',
    omc: '',
    city: '',
    state_name: '',
    phone_number: '',
    paytm_number: '',
    tin_number: '',
    gst_number: '',
    pin_code: '',
    isLoading: false,
    devicetoken: '',
    setValidation: false
  });

  useEffect(() => {
    LocalStorage.getFcmToken('fcmToken').then((fcmToken) => {
      setState({ ...state, devicetoken: fcmToken })
      getDropDataList();
    })
  }, [])

  const getDropDataList = async () => {
    const response = await Api.getMOC();
    const { status = false, data = [] } = response;
    if (status) {
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        var obj = { "label": element.name, "value": element.id }
        selectJob.push(obj);
      }
    }
  }

  const onSubmitHandler = async () => {

    const {
      name,
      contact_person,
      proprietor_name,
      email_address,
      omc,
      city,
      state_name,
      phone_number,
      paytm_number,
      tin_number,
      gst_number,
      pin_code,
      devicetoken,
      setValidation
    } = state;

    if (name === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (contact_person === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (proprietor_name === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (email_address === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (!validateEmail(email_address)) {
      // setState({ ...state, setValidation: true });
      Toast.show('Please enter your valid email address')
      return;
    }

    if (omc === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (city === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (state_name === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (!phone_number) {
      // setState({ ...state, setValidation: true });
      Toast.show('Please enter your valid phone number')
      return;
    }

    if (phone_number.length !== 10) {
      // setState({ ...state, setValidation: true });
      Toast.show('Mobile number must be in 10 digits')
      return;
    }

    if (paytm_number === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (paytm_number.length !== 10) {
      // setState({ ...state, setValidation: true });
      Toast.show('Paytm number must be in 10 digits')
      return;
    }

    if (tin_number === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (gst_number === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    if (pin_code === '') {
      setState({ ...state, setValidation: true });
      return;
    }

    console.log('------body: ')
    const body = {
      name: name,
      contact_person: contact_person,
      proprietor_name: proprietor_name,
      email: email_address,
      omc: omc,
      city: city,
      state: state_name,
      phone: phone_number,
      paytm_number: paytm_number,
      tin_no: tin_number,
      gst_no: gst_number,
      pincode: pin_code,
      bank_name: '',
      account_holder_name: '',
      branch_name: '',
      account_number: '',
      ifsc_code: '',
      latitude: '',
      longitude: '',
      selling_petrol: '',
      selling_diesel: '',
      password: '',
      device_type: Platform.OS === 'ios' ? 'IOS' : "Android",
      devicetoken: devicetoken,
    };
    const bodyCheck = {
      email: email_address,
      phone: phone_number
    }
    console.log('------body: ', body)
    setState({ ...state, isLoading: true });
    console.log('------body 2 : ', bodyCheck)
    const response = await Api.checkEmailPhone(bodyCheck);
    // setState({ ...state, isLoading: false });
    console.log('------response : ', response)
    const {
      status = false,
      msg
    } = response;
    if (status) {
      setState({ ...state, isLoading: false });
      console.log('------body: ', body)
      navigation.replace('Register1', { item: body });
    } else {
      const {
        data: { msg = 'Something went wrong' },
      } = response;
      setState({ ...state, isLoading: false });
      Toast.show(msg)
    }
  };

  const onChangeDropdownUserType = (label, index, data) => {
    setState({ ...state, omc: data[index].label });
  };

  return (
    <KeyboardAwareScrollView>
      <MainImage height={"80.50%"} bottom={"0%"}>
        <BottomView>
          <Text style={styles.topText}>Personal Detail</Text>
          <Text style={styles.topText_1}>Enter your basic details</Text>

          <CustomInputField
            tfSource={require('../images/man-user.png')}
            label="Name"
            width={16}
            height={16}
            value={state.name}
            onChangeText={name => {
              setState({ ...state, name });
              // setState({ ...state, setValidation: false });
            }}
          />
          {state.setValidation == true && state.name === '' ?
            <Text style={styles.validationStyle}>Please enter your name</Text>
            : <></>
          }
          <CustomInputField
            tfSource={require('../images/man-user.png')}
            label="Contact Person"
            width={16}
            height={16}
            value={state.contact_person}
            onChangeText={contact_person =>
              setState({ ...state, contact_person })
            }
          />
          {state.setValidation == true && state.contact_person === '' ?
            <Text style={styles.validationStyle}>Please enter your contact person</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/building.png')}
            label="Proprietor Name"
            width={16}
            height={16}
            value={state.proprietor_name}
            onChangeText={proprietor_name =>
              setState({ ...state, proprietor_name })
            }
          />
          {state.setValidation == true && state.proprietor_name === '' ?
            <Text style={styles.validationStyle}>Please enter your proprietor name</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/email.png')}
            label="Email Address"
            width={16}
            height={16}
            // autoCorrect={false}
            autoCapitalize="none"
            keyboardType={'email-address'}
            value={state.email_address}
            onChangeText={email_address => setState({ ...state, email_address })}
          />
          {state.setValidation == true && state.email_address === '' ?
            <Text style={styles.validationStyle}>Please enter your email address</Text>
            : <></>
          }

          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ width: "10%", justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 16, height: 16 }} source={require('../images/omc.png')} />
            </View>
            <View style={{ flex: 1, }}>
              <Dropdown
                style={styles.drops}
                itemColor={'rgba(0, 0, 0, .54)'}
                underlineColor="transparent"
                label={'OMC'}
                // icon="cheveron-down"
                iconColor="rgba(0, 0, 0, 1)"
                icon={require('../images/next.png')}
                dropdownOffset={{ top: 32, left: 0 }}
                dropdownMargins={{ min: 8, max: 16 }}
                pickerStyle={{ width: '88%', left: '6%', marginTop: 15 }}
                dropdownPosition={-3.5}
                shadeOpacity={0.12}
                rippleOpacity={0.4}
                baseColor={'white'}
                data={selectJob}
                absoluteRTLLayout={true}
                onChangeText={(label, index, data) => {
                  onChangeDropdownUserType(label, index, data);
                }}
              />
            </View>
          </View>
          {state.setValidation == true && state.omc === '' ?
            <Text style={styles.validationStyle}>Please enter your omc</Text>
            : <></>
          }
          <CustomInputField
            tfSource={require('../images/building.png')}
            label="City"
            width={16}
            height={16}
            value={state.city}
            onChangeText={city => setState({ ...state, city })}
          />
          {state.setValidation == true && state.city === '' ?
            <Text style={styles.validationStyle}>Please enter your city</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/state.png')}
            label="State"
            width={18}
            height={18}
            value={state.state_name}
            onChangeText={state_name => setState({ ...state, state_name })}
          />
          {state.setValidation == true && state.state_name === '' ?
            <Text style={styles.validationStyle}>Please enter your state</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/phone.png')}
            label="Phone Number"
            width={16}
            height={16}
            value={state.phone_number}
            keyboardType={'number-pad'}
            maxLength={10}
            onChangeText={phone_number => setState({ ...state, phone_number })}
          />
          {state.setValidation == true && state.phone_number === '' || state.phone_number.length > 10 ?
            <Text style={styles.validationStyle}>Please enter your valid phone number</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/phone.png')}
            label="Paytm Number"
            width={16}
            height={16}
            value={state.paytm_number}
            keyboardType={'number-pad'}
            maxLength={10}
            onChangeText={paytm_number => setState({ ...state, paytm_number })}
          />
          {state.setValidation == true && state.paytm_number === '' ?
            <Text style={styles.validationStyle}>Please enter your paytm number</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/tax.png')}
            label="Tin Number"
            width={16}
            height={16}
            value={state.tin_number}
            keyboardType={'number-pad'}
            onChangeText={tin_number => setState({ ...state, tin_number })}
          />
          {state.setValidation == true && state.tin_number === '' ?
            <Text style={styles.validationStyle}>Please enter your tin number</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/tax.png')}
            label="Gst Number"
            width={16}
            height={16}
            value={state.gst_number}
            onChangeText={gst_number => setState({ ...state, gst_number })}
          />
          {state.setValidation == true && state.gst_number === '' ?
            <Text style={styles.validationStyle}>Please enter your gst number</Text>
            : <></>
          }

          <CustomInputField
            tfSource={require('../images/place.png')}
            label="Pin Code"
            width={16}
            height={16}
            value={state.pin_code}
            keyboardType={'number-pad'}
            onChangeText={pin_code => setState({ ...state, pin_code })}
          />
          {state.setValidation == true && state.pin_code === '' ?
            <Text style={styles.validationStyle}>Please enter your pin code</Text>
            : <></>
          }

          <BottomButton
            loader={state.isLoading}
            bottomtitle="SAVE & NEXT"
            onPress={() => { onSubmitHandler() }}
          />

          <TouchableOpacity >
            <Text style={styles.bottomText}>
              Already have an account?
              <Text
                onPress={() => { navigation.navigate('Login') }}
                style={styles.bottomText_1}> Login Now</Text>
            </Text>
          </TouchableOpacity>
        </BottomView>
      </MainImage>
    </KeyboardAwareScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  topText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0E1236',
    marginHorizontal: 10,
  },
  topText_1: {
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
    fontWeight: '400',
    color: '#8D92A3',
    marginLeft: 10,
    marginTop: 4,
  },
  otpInput: {
    // width: '84%',
    padding: 10,
    marginHorizontal: 10,
    marginTop: 20,
    height: 66,
    alignSelf: 'center',
  },
  bottomText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#000521',
    alignSelf: 'center',
    marginTop: 10,
  },
  bottomText_1: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ED6E1E',
    alignSelf: 'center',
    marginTop: 4,
  },
  validationStyle: {
    fontFamily: 'Avenir-Medium', fontSize: 14, fontWeight: '500', color: '#FA7272', marginHorizontal: 35
  },
  drops: {
    height: 50,
    width: "95%",
    // paddingRight:width*.14,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#00000040',
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#000521',
    paddingHorizontal: 1,
  },
});
