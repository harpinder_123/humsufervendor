const data = {
  userDetail: {},
  latlongInfo: '',
  longInfo: '',
  deviceInfo: {
    id: '',
    token: '',
    model: '',
    os: '',
  },
};
const reducer = (state = data, action) => {
  switch (action.type) {
    case 'setUserDetail':
      return {
        ...state,
        userDetail: action.payload,
        isLogin: true,
      };
    case 'latlongInfo':
      return {
        ...state,
        latlongInfo: action.payload,
      };
    case 'longInfo':
      return {
        ...state,
        longInfo: action.payload,
      };
    default:
      return state;
  }
};
export default reducer;
