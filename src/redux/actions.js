export const SetDeviceInfo = deviceInfo => ({
  type: 'SetDeviceInfo',
  payload: deviceInfo,
});
export const SetUserDetail = payload => ({ type: 'SetUserDetail', payload });
export const LatlongInfo = payload => ({
  type: 'latlongInfo',
  payload,
});
export const LongInfo = payload => ({
  type: 'longInfo',
  payload,
});