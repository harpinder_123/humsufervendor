import * as React from 'react';
import { CardStyleInterpolators } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Splash from '../screens/Splash';
import Login from '../screens/Login';
import Register from '../screens/Register';
import Register1 from '../screens/Register1';
import Register2 from '../screens/Register2';
import Otp from '../screens/Otp';
import DieselPrice from '../screens/DieselPrice';
import NewOrder from '../screens/NewOrder';
import Reject from '../screens/Reject';
import SelectDriver from '../screens/SelectDriver';
import Approval from '../screens/Approval';
import DrawerNavigator from './DrawerNavigator';
import MyDrivers from '../screens/MyDrivers';
import Notification from '../screens/Notification';
import EditProfile from '../screens/EditProfile';
import Pending from '../screens/Pending';
import Topup from '../screens/Topup';
import UpdateDriver from '../screens/UpdateDriver';
import Support from '../screens/Support';
import DriverList from '../screens/DriverList';
import CreateDriver from '../screens/CreateDriver';
import Home from '../screens/Home';
import DriverDetails from '../screens/DriverDetails';
import OrderDetail from '../screens/OrderDetail';
import QRCodeScreen from '../screens/QRCodeScreen';
import TransactionHistory from '../screens/TransactionHistory';
import WebViewScreen from '../screens/WebViewScreen';
import UpdateFuelPrice from '../screens/UpdateFuelPrice';
import TransactionHistoryVander from '../screens/TransactionHistoryVander';
import RegistApproval from '../screens/RegistApproval'

const Stack = createNativeStackNavigator();
const StackNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          headerShown: false,
          cardStyleInterpolators: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Register1" component={Register1} />
        <Stack.Screen name="Register2" component={Register2} />
        <Stack.Screen name="Otp" component={Otp} />
        <Stack.Screen name="DieselPrice" component={DieselPrice} />
        <Stack.Screen name="NewOrder" component={NewOrder} />
        <Stack.Screen name="Reject" component={Reject} />
        <Stack.Screen name="SelectDriver" component={SelectDriver} />
        <Stack.Screen name="Approval" component={Approval} />
        <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="DriverDetails" component={DriverDetails} />
        <Stack.Screen name="MyDrivers" component={MyDrivers} />
        <Stack.Screen name="Notification" component={Notification} />
        <Stack.Screen name="EditProfile" component={EditProfile} />
        <Stack.Screen name="Pending" component={Pending} />
        <Stack.Screen name="Topup" component={Topup} />
        <Stack.Screen name="UpdateDriver" component={UpdateDriver} />
        <Stack.Screen name="Support" component={Support} />
        <Stack.Screen name="DriverList" component={DriverList} />
        <Stack.Screen name="CreateDriver" component={CreateDriver} />
        <Stack.Screen name="OrderDetail" component={OrderDetail} />
        <Stack.Screen name="QRCodeScreen" component={QRCodeScreen} />
        <Stack.Screen name="TransactionHistory" component={TransactionHistory} />
        <Stack.Screen name="WebViewScreen" component={WebViewScreen} />
        <Stack.Screen name="UpdateFuelPrice" component={UpdateFuelPrice} />
        <Stack.Screen name="TransactionHistoryVander" component={TransactionHistoryVander} />
        <Stack.Screen name="RegistApproval" component={RegistApproval} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default StackNavigator;
