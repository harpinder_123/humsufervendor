import * as React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawerContent from '../screens/CustomDrawerNavigator';
import Home from '../screens/Home';

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => (
  <Drawer.Navigator
    screenOptions={{headerShown: false}}
    initialRouteName="Home"
    drawerStyle={{width: '70%'}}
    drawerContent={props => <CustomDrawerContent {...props} />}>
    <Drawer.Screen name="Home" component={Home} />
  </Drawer.Navigator>
);
export default DrawerNavigator;
