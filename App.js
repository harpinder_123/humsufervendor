import React, { useEffect, useState } from 'react';
import { LogBox } from 'react-native';
import StackNavigator from './src/navigator/StackNavigator';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import * as actions from './src/redux/actions';
import { enableFreeze } from 'react-native-screens';

enableFreeze(true);

LogBox.ignoreAllLogs();//Ignore all log notifications
window.consolejson = json => console.log(JSON.stringify(json, null, 2));

const App = ({ navigation }) => {

  return (
    <Provider store={store}>
      <StackNavigator />
    </Provider>
  );
};

export default App;
