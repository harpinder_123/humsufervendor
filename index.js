import { AppRegistry, Alert, Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
// import RNVoipCall from 'react-native-voip-call';
import App from './App';
import { name as appName } from './app.json';
import { EventRegister } from 'react-native-event-listeners';
import PushNotification from "react-native-push-notification";


messaging().setBackgroundMessageHandler(async remoteMessage => {
    EventRegister.emit('myCustomEvent', 'it works!!!');
    console.log('Message handled in the background!', remoteMessage);
    if (Platform.OS === 'android') {
        let data;
        if (remoteMessage.data) {
            data = remoteMessage.data;
        }

        let callOptions = {
            callerId: '1', // Important uuid must in this formats
            android: {
                ringtuneSound: true, // defualt true
                ringtune: 'ringtune', // add file inside Project_folder/android/app/res/raw --Formats--> mp3,wav
                duration: 5000, // defualt 30000
                vibration: true, // defualt is true
                channel_name: 'call', //
                notificationId: 1123,
                notificationTitle: 'New Booking',
                notificationBody: 'You have new Booking ',
                answerActionTitle: 'Answer',
                declineActionTitle: 'Decline',
            },
        };
        // RNVoipCall.displayIncomingCall(callOptions)
        //     .then(data => {
        //         console.log(data);
        //     })
        //     .catch(e => console.log(e));
    }
});
//receive the message in the background
// messaging().onMessage(async teste => {
//     EventRegister.emit('myCustomEvent', 'it works!!!');
//     console.log('teste :', teste);
//     let notiData = teste.notification;
//     PushNotification.localNotification({
//         channelId: 'default_channel_1',
//         title: notiData.title, // (optional)
//         message: notiData.body, // (required)
//         vibrate: true,
//         vibration: 300,
//         playSound: true,
//         soundName: 'my_sound.mp3',
//     })
//     // const novo = (title, message) => {
//     //     return Alert.alert(title, message);
//     // };

//     // novo('Humsafr Vendor', 'You have new order');

// });
AppRegistry.registerComponent(appName, () => App);
